// SPDX-License-Identifier: MIT

#include <oqs/sig.h>

#if defined(OQS_SIG_alg_rainier3_L1)

#include <string.h>
#include <stdio.h>
#include <oqs/common.h>
#include "sig_rainier.h"
#include "external/c_bindings.h"

#ifdef OQS_ENABLE_SIG_rainier3_L1

OQS_SIG *OQS_SIG_rainier3_L1_new() {
	OQS_SIG *sig = malloc(sizeof(OQS_SIG));
	if (sig == NULL) {
		return NULL;
	}
	sig->method_name = OQS_SIG_alg_rainier3_L1;
	sig->alg_version = "https://github.com/IAIK/rainier-signatures";

	sig->claimed_nist_level = 1;
	sig->euf_cma = true;

	sig->length_public_key = OQS_SIG_rainier3_L1_length_public_key;
	sig->length_secret_key = OQS_SIG_rainier3_L1_length_secret_key;
	sig->length_signature = OQS_SIG_rainier3_L1_length_signature;

	sig->keypair = OQS_SIG_rainier3_L1_keypair;
	sig->sign = OQS_SIG_rainier3_L1_sign;
	sig->verify = OQS_SIG_rainier3_L1_verify;

	return sig;
}

OQS_API OQS_STATUS OQS_SIG_rainier3_L1_keypair(uint8_t *public_key, uint8_t *secret_key) {
	if (public_key == NULL || secret_key == NULL) {
		return OQS_ERROR;
	}

	uint8_t* keypair = rainier3_L1_keygen();
	if (keypair == NULL) {
		return OQS_ERROR;
	}

	memcpy(public_key, keypair, OQS_SIG_rainier3_L1_length_public_key);
	memcpy(secret_key, keypair, OQS_SIG_rainier3_L1_length_secret_key);

	// wipe public and private key
	OQS_MEM_cleanse(&keypair, sizeof(uint8_t));
	return OQS_SUCCESS;
}

OQS_API OQS_STATUS OQS_SIG_rainier3_L1_sign(uint8_t *signature, size_t *signature_len, const uint8_t *message, size_t message_len, const uint8_t *secret_key) {
	if (signature == NULL || signature_len == NULL || message == NULL || secret_key == NULL) {
		return OQS_ERROR;
	}
	
	uint8_t* signature_candidate = rainier_sign_c(RAINIER3_L1, secret_key, message, message_len, signature_len);
	if (signature_candidate == NULL) {
		return OQS_ERROR;
	}

	memcpy(signature, signature_candidate, OQS_SIG_rainier3_L1_length_signature);

	// wipe signature
	OQS_MEM_cleanse(&signature_candidate, sizeof(uint8_t*));
	return OQS_SUCCESS;
}

OQS_API OQS_STATUS OQS_SIG_rainier3_L1_verify(const uint8_t *message, size_t message_len, const uint8_t *signature, size_t signature_len, const uint8_t *public_key) {
	if (message == NULL || signature == NULL || public_key == NULL) {
		return OQS_ERROR;
	}
	
	if (rainier_verify_c(RAINIER3_L1, public_key, signature, signature_len, message, message_len) == true) {
		return OQS_SUCCESS;
	}
	
	return OQS_ERROR;
}

#endif

#ifdef OQS_ENABLE_SIG_rainier4_L1

OQS_SIG *OQS_SIG_rainier4_L1_new() {
	OQS_SIG *sig = malloc(sizeof(OQS_SIG));
	if (sig == NULL) {
		return NULL;
	}
	sig->method_name = OQS_SIG_alg_rainier4_L1;
	sig->alg_version = "https://github.com/IAIK/rainier-signatures";

	sig->claimed_nist_level = 1;
	sig->euf_cma = true;

	sig->length_public_key = OQS_SIG_rainier4_L1_length_public_key;
	sig->length_secret_key = OQS_SIG_rainier4_L1_length_secret_key;
	sig->length_signature = OQS_SIG_rainier4_L1_length_signature;

	sig->keypair = OQS_SIG_rainier4_L1_keypair;
	sig->sign = OQS_SIG_rainier4_L1_sign;
	sig->verify = OQS_SIG_rainier4_L1_verify;

	return sig;
}

OQS_API OQS_STATUS OQS_SIG_rainier4_L1_keypair(uint8_t *public_key, uint8_t *secret_key) {
	if (public_key == NULL || secret_key == NULL) {
		return OQS_ERROR;
	}

	uint8_t* keypair = rainier4_L1_keygen();
	if (keypair == NULL) {
		return OQS_ERROR;
	}

	memcpy(public_key, keypair, OQS_SIG_rainier4_L1_length_public_key);
	memcpy(secret_key, keypair, OQS_SIG_rainier4_L1_length_secret_key);

	// wipe public and private key
	OQS_MEM_cleanse(&keypair, sizeof(uint8_t));
	return OQS_SUCCESS;
}

OQS_API OQS_STATUS OQS_SIG_rainier4_L1_sign(uint8_t *signature, size_t *signature_len, const uint8_t *message, size_t message_len, const uint8_t *secret_key) {
	if (signature == NULL || signature_len == NULL || message == NULL || secret_key == NULL) {
		return OQS_ERROR;
	}
	
	uint8_t* signature_candidate = rainier_sign_c(RAINIER4_L1, secret_key, message, message_len, signature_len);
	if (signature_candidate == NULL) {
		return OQS_ERROR;
	}

	memcpy(signature, signature_candidate, OQS_SIG_rainier4_L1_length_signature);

	// wipe signature
	OQS_MEM_cleanse(&signature_candidate, sizeof(uint8_t*));
	return OQS_SUCCESS;
}

OQS_API OQS_STATUS OQS_SIG_rainier4_L1_verify(const uint8_t *message, size_t message_len, const uint8_t *signature, size_t signature_len, const uint8_t *public_key) {
	if (message == NULL || signature == NULL || public_key == NULL) {
		return OQS_ERROR;
	}
	
	if (rainier_verify_c(RAINIER4_L1, public_key, signature, signature_len, message, message_len) == true) {
		return OQS_SUCCESS;
	}
	
	return OQS_ERROR;
}

#endif

#endif