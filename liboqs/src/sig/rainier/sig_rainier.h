// SPDX-License-Identifier: MIT

#ifndef OQS_SIG_RAINIER_H
#define OQS_SIG_RAINIER_H

#include <oqs/oqs.h>

#ifdef OQS_ENABLE_SIG_rainier3_L1

#define OQS_SIG_rainier3_L1_length_public_key 32
#define OQS_SIG_rainier3_L1_length_secret_key 16 + 32
#define OQS_SIG_rainier3_L1_length_signature 6720

OQS_SIG *OQS_SIG_rainier3_L1_new(void);

OQS_API OQS_STATUS OQS_SIG_rainier3_L1_keypair(uint8_t *public_key, uint8_t *secret_key);
OQS_API OQS_STATUS OQS_SIG_rainier3_L1_sign(uint8_t *signature, size_t *signature_len, const uint8_t *message, size_t message_len, const uint8_t *secret_key);
OQS_API OQS_STATUS OQS_SIG_rainier3_L1_verify(const uint8_t *message, size_t message_len, const uint8_t *signature, size_t signature_len, const uint8_t *public_key);

#endif

#ifdef OQS_ENABLE_SIG_rainier4_L1

#define OQS_SIG_rainier4_L1_length_public_key 32
#define OQS_SIG_rainier4_L1_length_secret_key 16 + 32
#define OQS_SIG_rainier4_L1_length_signature 7456

OQS_SIG *OQS_SIG_rainier4_L1_new(void);

OQS_API OQS_STATUS OQS_SIG_rainier4_L1_keypair(uint8_t *public_key, uint8_t *secret_key);
OQS_API OQS_STATUS OQS_SIG_rainier4_L1_sign(uint8_t *signature, size_t *signature_len, const uint8_t *message, size_t message_len, const uint8_t *secret_key);
OQS_API OQS_STATUS OQS_SIG_rainier4_L1_verify(const uint8_t *message, size_t message_len, const uint8_t *signature, size_t signature_len, const uint8_t *public_key);

#endif

#endif // OQS_SIG_RAINIER_H