#include "c_bindings.h"
#include "instances.h"
#include "signature.hpp"

#include <iostream>
#include <vector>

uint8_t* rainier3_L1_keygen() 
{
  const signature_instance_t& instance = instance_get(Rainier_3_L1_Param1);
  const keypair_t keypair = rainier_keygen(instance);

  uint8_t* concatenated_keypair = (uint8_t*)calloc(sizeof(uint8_t), keypair.first.size() + keypair.second.size());
  if (concatenated_keypair == NULL) {
    return NULL;
  }

  std::copy(keypair.second.begin(), keypair.second.end(), concatenated_keypair);
  std::copy(keypair.first.begin(), keypair.first.end(), concatenated_keypair + keypair.second.size());

  return concatenated_keypair;
}

uint8_t* rainier4_L1_keygen() 
{
  const signature_instance_t& instance = instance_get(Rainier_4_L1_Param1);
  const keypair_t keypair = rainier_keygen(instance);

  uint8_t* concatenated_keypair = (uint8_t*)calloc(sizeof(uint8_t), keypair.first.size() + keypair.second.size());
  if (concatenated_keypair == NULL) {
    return NULL;
  }

  std::copy(keypair.second.begin(), keypair.second.end(), concatenated_keypair);
  std::copy(keypair.first.begin(), keypair.first.end(), concatenated_keypair + keypair.second.size());

  return concatenated_keypair;
}

uint8_t* rainier_sign_c(const algorithm_type algorithm_version,
                        const uint8_t* secret_key,
                        const uint8_t *message, 
                        size_t message_len,
                        size_t* signature_len)
{
  if (secret_key == NULL || message == NULL) {
    return NULL;
  }

  const params_t instance_type = algorithm_version == RAINIER3_L1 ? Rainier_3_L1_Param1 : Rainier_4_L1_Param1;
  const signature_instance_t& instance = instance_get(instance_type);

  const keypair_t converted_keypair{std::vector<uint8_t>(secret_key + instance.block_cipher_params.key_size * 2, secret_key + instance.block_cipher_params.key_size * 3), 
                                    std::vector<uint8_t>(secret_key, secret_key + instance.block_cipher_params.key_size * 2)};

  const std::vector<uint8_t> signature = rainier_sign(instance, converted_keypair, message, message_len);

  *signature_len = signature.size();

  uint8_t* converted_signature = (uint8_t*)calloc(sizeof(uint8_t), signature.size());
  if (converted_signature == NULL) {
    return NULL;
  }

  std::copy(signature.begin(), signature.end(), converted_signature);

  return converted_signature;
}

bool rainier_verify_c(const algorithm_type algorithm_version,
                      const uint8_t* public_key,
                      const uint8_t* signature,
                      size_t signature_len,
                      const uint8_t* message,
                      size_t message_len)
{
  const params_t instance_type = algorithm_version == RAINIER3_L1 ? Rainier_3_L1_Param1 : Rainier_4_L1_Param1;
  const signature_instance_t& instance = instance_get(instance_type);

  const std::vector<uint8_t> converted_signature(signature, signature + signature_len);
  const std::vector<uint8_t> converted_public_key(public_key, public_key + instance.block_cipher_params.key_size * 2);

  return rainier_verify(instance, converted_public_key, converted_signature, message, message_len);
}