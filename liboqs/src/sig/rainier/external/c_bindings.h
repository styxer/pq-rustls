#ifndef __C_BINDINGS_H
#define __C_BINDINGS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

enum type_t
{
    RAINIER3_L1 = 1,
    RAINIER4_L1 = 4
};

typedef enum type_t algorithm_type;

uint8_t* rainier3_L1_keygen();
uint8_t* rainier4_L1_keygen();

uint8_t* rainier_sign_c(const algorithm_type algorithm_version,
                        const uint8_t* secret_key,
                        const uint8_t *message, 
                        size_t message_len,
                        size_t* signature_len);

bool rainier_verify_c(const algorithm_type algorithm_version,
                      const uint8_t* public_key,
                      const uint8_t* signature,
                      size_t signature_len,
                      const uint8_t* message,
                      size_t message_len);

#ifdef __cplusplus
}
#endif
#endif