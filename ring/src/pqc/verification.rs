// Copyright 2015-2016 Brian Smith.
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHORS DISCLAIM ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

//! Verification of PQC signatures.

use crate::{ error, sealed, signature };
use oqs::sig;
use alloc::vec::Vec;

#[derive(Debug)]
pub struct PqcVerificationAlgorithm {
    algorithm: sig::Algorithm
}

impl sealed::Sealed for PqcVerificationAlgorithm {}

impl signature::VerificationAlgorithm for PqcVerificationAlgorithm {
    fn verify(
        &self,
        public_key: untrusted::Input,
        msg: untrusted::Input,
        signature: untrusted::Input,
    ) -> Result<(), error::Unspecified> {
        let sigalg = sig::Sig::new(self.algorithm).ok().ok_or(error::Unspecified)?;

        let pubkey = public_key.read_all(error::Unspecified, |reader| {
            let mut vec = Vec::new();
            for _ in 0..public_key.len() {
                vec.push(reader.read_byte().ok().ok_or(error::Unspecified)?);
            }

            Ok(vec)
        }).ok().ok_or(error::Unspecified)?;
        
        let msg = msg.read_all(error::Unspecified, |reader| {
            let mut vec = Vec::new();
            for _ in 0..msg.len() {
                vec.push(reader.read_byte().ok().ok_or(error::Unspecified)?);
            }

            Ok(vec)
        }).ok().ok_or(error::Unspecified)?;

        let signature = signature.read_all(error::Unspecified, |reader| {
            let mut vec = Vec::new();
            for _ in 0..signature.len() {
                vec.push(reader.read_byte().ok().ok_or(error::Unspecified)?);
            }

            Ok(vec)
        }).ok().ok_or(error::Unspecified)?;

        let sigalg_pubkey = sigalg.public_key_from_bytes(pubkey.as_ref()).ok_or(error::Unspecified)?;
        let sigalg_signature = sigalg.signature_from_bytes(signature.as_ref()).ok_or(error::Unspecified)?;
        
        sigalg.verify(msg.as_ref(), sigalg_signature, sigalg_pubkey).ok().ok_or(error::Unspecified)
    }
}

/// Dilithium2 verification algorithm
pub static DILITHIUM2: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Dilithium2
};

/// Dilithium3 verification algorithm
pub static DILITHIUM3: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Dilithium3
};

/// Dilithium5 verification algorithm
pub static DILITHIUM5: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Dilithium5
};

/// Falcon512 verification algorithm
pub static FALCON512: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Falcon512
};

/// Falcon1024 verification algorithm
pub static FALCON1024: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Falcon1024
};

/// RainbowIClassic verification algorithm
pub static RAINBOW_I_CLASSIC: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowIClassic
};

/// RainbowICircumzenithal verification algorithm
pub static RAINBOW_I_CIRCUMZENITHAL: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowICircumzenithal
};

/// RainbowICompressed verification algorithm
pub static RAINBOW_I_COMPRESSED: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowICompressed
};

/// RainbowIiiClassic verification algorithm
pub static RAINBOW_III_CLASSIC: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowIiiClassic
};

/// RainbowIiiCircumzenithal verification algorithm
pub static RAINBOW_III_CIRCUMZENITHAL: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowIiiCircumzenithal
};

/// RainbowIiiCompressed verification algorithm
pub static RAINBOW_III_COMPRESSED: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowIiiCompressed
};

/// RainbowVClassic verification algorithm
pub static RAINBOW_V_CLASSIC: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowVClassic
};

/// RainbowVCircumzenithal verification algorithm
pub static RAINBOW_V_CIRCUMZENITHAL: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowVCircumzenithal
};

/// RainbowVCompressed verification algorithm
pub static RAINBOW_V_COMPRESSED: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::RainbowVCompressed
};

/// PicnicL1Fs verification algorithm
pub static PICNIC_L1_FS: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::PicnicL1Fs
};

/// PicnicL1Ur verification algorithm
pub static PICNIC_L1_UR: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::PicnicL1Ur
};

/// PicnicL1Full verification algorithm
pub static PICNIC_L1_FULL: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::PicnicL1Full
};

/// Picnic3L1 verification algorithm
pub static PICNIC3_L1: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Picnic3L1
};

/// Picnic3L1 verification algorithm
pub static PICNIC3_L3: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Picnic3L3
};

/// Picnic3L1 verification algorithm
pub static PICNIC3_L5: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Picnic3L5
};

/// Rainier3L1 verification algorithm
pub static RAINIER3_L1: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Rainier3L1
};

/// Rainier4L1 verification algorithm
pub static RAINIER4_L1: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::Rainier4L1
};

/// SphincsSha256128fRobust verification algorithm
pub static SPHINCS_SHA_256_128F_ROBUST: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::SphincsSha256128fRobust
};

/// SphincsSha256128fSimple verification algorithm
pub static SPHINCS_SHA_256_128F_SIMPLE: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::SphincsSha256128fSimple
};

/// SphincsSha256128sRobust verification algorithm
pub static SPHINCS_SHA_256_128S_ROBUST: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::SphincsSha256128sRobust
};

/// SphincsSha256128sSimple verification algorithm
pub static SPHINCS_SHA_256_128S_SIMPLE: PqcVerificationAlgorithm = PqcVerificationAlgorithm {
    algorithm: sig::Algorithm::SphincsSha256128sSimple
};
