use oqs::sig;

use crate::{
    error,
    io::der,
    pkcs8
};

use untrusted;

/// PQC public and secret keys
pub struct PqcKeyPair {
    /// public key
    pub public_key :sig::PublicKey,
    /// secret key
    secret_key :sig::SecretKey
}

impl PqcKeyPair {

    /// Extracts the public and secret key from PKCS#8 data
    pub fn from_pkcs8(pkcs8: &[u8], algorithm: sig::Algorithm) -> Result<Self, error::KeyRejected> {
        let secret_key =
            unwrap_pkcs8(pkcs8::Version::V1OrV2, untrusted::Input::from(pkcs8), algorithm)?;
        Self::from_secret_key_public_key(
            algorithm,
            secret_key.as_slice_less_safe()
        )
    }

    fn from_secret_key_public_key(algorithm :sig::Algorithm, secret_key: &[u8]) -> Result<Self, error::KeyRejected> {
        let sigalg = sig::Sig::new(algorithm).ok().ok_or(error::KeyRejected::invalid_encoding())?;        
        if secret_key.len() - sigalg.length_secret_key() != sigalg.length_public_key() {
            return Err(error::KeyRejected::invalid_component());
        }

        let public_key = secret_key[sigalg.length_secret_key()..].to_vec();
        let secret_key = secret_key[0..sigalg.length_secret_key()].to_vec();
        
        let public_key = sigalg.public_key_from_bytes(&public_key).ok_or(error::KeyRejected::wrong_algorithm())?;
        let secret_key = sigalg.secret_key_from_bytes(&secret_key).ok_or(error::KeyRejected::wrong_algorithm())?;        

        Ok(Self{
            public_key: public_key.to_owned(),
            secret_key: secret_key.to_owned()
        })
    }

    /// Signs the given message with the secret key and the given algorithm
    pub fn sign(&self, msg: &[u8], algorithm :sig::Algorithm) -> Result<sig::Signature, error::Unspecified> {
        let sigalg = sig::Sig::new(algorithm).ok().ok_or(error::Unspecified)?;
        sigalg.sign(msg, &self.secret_key).map_err(|_| error::Unspecified)
    }
}

fn unwrap_pkcs8(
    version: pkcs8::Version,
    input: untrusted::Input,
    algorithm: sig::Algorithm
) -> Result<untrusted::Input, error::KeyRejected> {
    let (secret_key, _) = match algorithm {
        sig::Algorithm::Dilithium2 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-dilithium2.der")), version, input)?,
        sig::Algorithm::Dilithium3 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-dilithium3.der")), version, input)?,
        sig::Algorithm::Dilithium5 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-dilithium5.der")), version, input)?,
        sig::Algorithm::Falcon512 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-falcon512.der")), version, input)?,
        sig::Algorithm::Falcon1024 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-falcon1024.der")), version, input)?,
        sig::Algorithm::RainbowIClassic => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-i-classic.der")), version, input)?,
        sig::Algorithm::RainbowICircumzenithal => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-i-circumzenithal.der")), version, input)?,
        sig::Algorithm::RainbowICompressed => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-i-compressed.der")), version, input)?,
        sig::Algorithm::RainbowIiiClassic => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-iii-classic.der")), version, input)?,
        sig::Algorithm::RainbowIiiCircumzenithal => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-iii-circumzenithal.der")), version, input)?,
        sig::Algorithm::RainbowIiiCompressed => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-iii-compressed.der")), version, input)?,
        sig::Algorithm::RainbowVClassic => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-v-classic.der")), version, input)?,
        sig::Algorithm::RainbowVCircumzenithal => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-v-circumzenithal.der")), version, input)?,
        sig::Algorithm::RainbowVCompressed => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainbow-v-compressed.der")), version, input)?,
        sig::Algorithm::Picnic3L1 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-picnic3-l1.der")), version, input)?,
        sig::Algorithm::Picnic3L3 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-picnic3-l3.der")), version, input)?,
        sig::Algorithm::Picnic3L5 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-picnic3-l5.der")), version, input)?,
        sig::Algorithm::PicnicL1Fs => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-picnic-l1-fs.der")), version, input)?,
        sig::Algorithm::PicnicL1Ur => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-picnic-l1-ur.der")), version, input)?,
        sig::Algorithm::PicnicL1Full => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-picnic-l1-full.der")), version, input)?,
        sig::Algorithm::Rainier3L1 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainier3-l1.der")), version, input)?,
        sig::Algorithm::Rainier4L1 => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-rainier4-l1.der")), version, input)?,
        sig::Algorithm::SphincsSha256128fRobust => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-sphincs-sha-256-128f-robust.der")), version, input)?,
        sig::Algorithm::SphincsSha256128fSimple => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-sphincs-sha-256-128f-simple.der")), version, input)?,
        sig::Algorithm::SphincsSha256128sRobust => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-sphincs-sha-256-128s-robust.der")), version, input)?,
        sig::Algorithm::SphincsSha256128sSimple => pkcs8::unwrap_key_(untrusted::Input::from(include_bytes!("alg-sphincs-sha-256-128s-simple.der")), version, input)?,
        _ => pkcs8::unwrap_key_(untrusted::Input::from(&[0]), version, input)?
    };
    let secret_key = secret_key
        .read_all(error::Unspecified, |input| {
            der::expect_tag_and_get_value(input, der::Tag::OctetString)
        })
        .map_err(|error::Unspecified| error::KeyRejected::invalid_encoding())?;
    Ok(secret_key)
}
