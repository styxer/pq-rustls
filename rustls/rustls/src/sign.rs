use crate::error::TLSError;
use crate::key;
use crate::msgs::enums::{SignatureAlgorithm, SignatureScheme};

use ring::{
    self,
    signature::{self, EcdsaKeyPair, Ed25519KeyPair, RsaKeyPair, PqcKeyPair},
};
use webpki;

use std::mem;
use std::sync::Arc;

use oqs::sig;

use std::convert::TryFrom;

/// An abstract signing key.
pub trait SigningKey: Send + Sync {
    /// Choose a `SignatureScheme` from those offered.
    ///
    /// Expresses the choice by returning something that implements `Signer`,
    /// using the chosen scheme.
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>>;

    /// What kind of key we have.
    fn algorithm(&self) -> SignatureAlgorithm;
}

/// A thing that can sign a message.
pub trait Signer: Send + Sync {
    /// Signs `message` using the selected scheme.
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError>;

    /// Reveals which scheme will be used when you call `sign()`.
    fn get_scheme(&self) -> SignatureScheme;
}

/// A packaged-together certificate chain, matching `SigningKey` and
/// optional stapled OCSP response and/or SCT list.
#[derive(Clone)]
pub struct CertifiedKey {
    /// The certificate chain.
    pub cert: Vec<key::Certificate>,

    /// The certified key.
    pub key: Arc<Box<dyn SigningKey>>,

    /// An optional OCSP response from the certificate issuer,
    /// attesting to its continued validity.
    pub ocsp: Option<Vec<u8>>,

    /// An optional collection of SCTs from CT logs, proving the
    /// certificate is included on those logs.  This must be
    /// a `SignedCertificateTimestampList` encoding; see RFC6962.
    pub sct_list: Option<Vec<u8>>,
}

impl CertifiedKey {
    /// Make a new CertifiedKey, with the given chain and key.
    ///
    /// The cert chain must not be empty. The first certificate in the chain
    /// must be the end-entity certificate.
    pub fn new(cert: Vec<key::Certificate>, key: Arc<Box<dyn SigningKey>>) -> CertifiedKey {
        CertifiedKey {
            cert,
            key,
            ocsp: None,
            sct_list: None,
        }
    }

    /// The end-entity certificate.
    pub fn end_entity_cert(&self) -> Result<&key::Certificate, ()> {
        self.cert.get(0).ok_or(())
    }

    /// Steal ownership of the certificate chain.
    pub fn take_cert(&mut self) -> Vec<key::Certificate> {
        mem::replace(&mut self.cert, Vec::new())
    }

    /// Return true if there's an OCSP response.
    pub fn has_ocsp(&self) -> bool {
        self.ocsp.is_some()
    }

    /// Steal ownership of the OCSP response.
    pub fn take_ocsp(&mut self) -> Option<Vec<u8>> {
        mem::replace(&mut self.ocsp, None)
    }

    /// Return true if there's an SCT list.
    pub fn has_sct_list(&self) -> bool {
        self.sct_list.is_some()
    }

    /// Steal ownership of the SCT list.
    pub fn take_sct_list(&mut self) -> Option<Vec<u8>> {
        mem::replace(&mut self.sct_list, None)
    }

    /// Check the certificate chain for validity:
    /// - it should be non-empty list
    /// - the first certificate should be parsable as a x509v3,
    /// - the first certificate should quote the given server name
    ///   (if provided)
    ///
    /// These checks are not security-sensitive.  They are the
    /// *server* attempting to detect accidental misconfiguration.
    pub fn cross_check_end_entity_cert(
        &self,
        name: Option<webpki::DnsNameRef>,
    ) -> Result<(), TLSError> {
        // Always reject an empty certificate chain.
        let end_entity_cert = self.end_entity_cert().map_err(|()| {
            TLSError::General("No end-entity certificate in certificate chain".to_string())
        })?;

        // Reject syntactically-invalid end-entity certificates.
        let end_entity_cert =
            webpki::EndEntityCert::try_from(end_entity_cert.as_ref()).map_err(|_| {
                TLSError::General(
                    "End-entity certificate in certificate \
                                  chain is syntactically invalid"
                        .to_string(),
                )
            })?;

        if let Some(name) = name {
            // If SNI was offered then the certificate must be valid for
            // that hostname. Note that this doesn't fully validate that the
            // certificate is valid; it only validates that the name is one
            // that the certificate is valid for, if the certificate is
            // valid.
            if end_entity_cert
                .verify_is_valid_for_dns_name(name)
                .is_err()
            {
                return Err(TLSError::General(
                    "The server certificate is not \
                                             valid for the given name"
                        .to_string(),
                ));
            }
        }

        Ok(())
    }
}

/// Parse `der` as any supported key encoding/type, returning
/// the first which works.
pub fn any_supported_type(der: &key::PrivateKey) -> Result<Box<dyn SigningKey>, ()> {
    if let Ok(rsa) = RSASigningKey::new(der) {
        Ok(Box::new(rsa))
    } else if let Ok(ecdsa) = any_ecdsa_type(der) {
        Ok(ecdsa)
    } else if let Ok(dilithium2) = Dilithium2SigningKey::new(der) {
        Ok(Box::new(dilithium2))
    }
    else if let Ok(dilithium3) = Dilithium3SigningKey::new(der) {
        Ok(Box::new(dilithium3))
    }
    else if let Ok(dilithium5) = Dilithium5SigningKey::new(der) {
        Ok(Box::new(dilithium5))
    }
    else if let Ok(falcon512) = Falcon512SigningKey::new(der) {
        Ok(Box::new(falcon512))
    }
    else if let Ok(falcon1024) = Falcon1024SigningKey::new(der) {
        Ok(Box::new(falcon1024))
    }
    else if let Ok(rainbow_i_classic) = RainbowIClassicSigningKey::new(der) {
        Ok(Box::new(rainbow_i_classic))
    }
    else if let Ok(rainbow_i_circumzenithal) = RainbowICircumzenithalSigningKey::new(der) {
        Ok(Box::new(rainbow_i_circumzenithal))
    }
    else if let Ok(rainbow_i_compressed) = RainbowICompressedSigningKey::new(der) {
        Ok(Box::new(rainbow_i_compressed))
    }
    else if let Ok(rainbow_iii_classic) = RainbowIiiClassicSigningKey::new(der) {
        Ok(Box::new(rainbow_iii_classic))
    }
    else if let Ok(rainbow_iii_circumzenithal) = RainbowIiiCircumzenithalSigningKey::new(der) {
        Ok(Box::new(rainbow_iii_circumzenithal))
    }
    else if let Ok(rainbow_iii_compressed) = RainbowIiiCompressedSigningKey::new(der) {
        Ok(Box::new(rainbow_iii_compressed))
    }
    else if let Ok(rainbow_v_classic) = RainbowVClassicSigningKey::new(der) {
        Ok(Box::new(rainbow_v_classic))
    }
    else if let Ok(rainbow_v_circumzenithal) = RainbowVCircumzenithalSigningKey::new(der) {
        Ok(Box::new(rainbow_v_circumzenithal))
    }
    else if let Ok(rainbow_v_compressed) = RainbowVCompressedSigningKey::new(der) {
        Ok(Box::new(rainbow_v_compressed))
    }
    else if let Ok(picnic_l1_fs) = PicnicL1FsSigningKey::new(der) {
        Ok(Box::new(picnic_l1_fs))
    }
    else if let Ok(picnic_l1_ur) = PicnicL1UrSigningKey::new(der) {
        Ok(Box::new(picnic_l1_ur))
    }
    else if let Ok(picnic_l1_full) = PicnicL1FullSigningKey::new(der) {
        Ok(Box::new(picnic_l1_full))
    }
    else if let Ok(picnic3_l1) = Picnic3L1SigningKey::new(der) {
        Ok(Box::new(picnic3_l1))
    }
    else if let Ok(picnic3_l3) = Picnic3L3SigningKey::new(der) {
        Ok(Box::new(picnic3_l3))
    }
    else if let Ok(picnic3_l5) = Picnic3L5SigningKey::new(der) {
        Ok(Box::new(picnic3_l5))
    }
    else if let Ok(rainier3_l1) = Rainier3L1SigningKey::new(der) {
        Ok(Box::new(rainier3_l1))
    }
    else if let Ok(rainier4_l1) = Rainier4L1SigningKey::new(der) {
        Ok(Box::new(rainier4_l1))
    } 
    else if let Ok(sphincs_256_128f_robust) = SphincsSha256128fRobustSigningKey::new(der) {
        Ok(Box::new(sphincs_256_128f_robust))
    }
    else if let Ok(sphincs_256_128f_simple) = SphincsSha256128fSimpleSigningKey::new(der) {
        Ok(Box::new(sphincs_256_128f_simple))
    }
    else if let Ok(sphincs_256_128s_robust) = SphincsSha256128sRobustSigningKey::new(der) {
        Ok(Box::new(sphincs_256_128s_robust))
    }
    else if let Ok(sphincs_256_128s_simple) = SphincsSha256128sSimpleSigningKey::new(der) {
        Ok(Box::new(sphincs_256_128s_simple))
    }
    else {
        any_eddsa_type(der)
    }
}

/// Parse `der` as any ECDSA key type, returning the first which works.
pub fn any_ecdsa_type(der: &key::PrivateKey) -> Result<Box<dyn SigningKey>, ()> {
    if let Ok(ecdsa_p256) = ECDSASigningKey::new(
        der,
        SignatureScheme::ECDSA_NISTP256_SHA256,
        &signature::ECDSA_P256_SHA256_ASN1_SIGNING,
    ) {
        return Ok(Box::new(ecdsa_p256));
    }

    if let Ok(ecdsa_p384) = ECDSASigningKey::new(
        der,
        SignatureScheme::ECDSA_NISTP384_SHA384,
        &signature::ECDSA_P384_SHA384_ASN1_SIGNING,
    ) {
        return Ok(Box::new(ecdsa_p384));
    }

    Err(())
}

/// Parse `der` as any EdDSA key type, returning the first which works.
pub fn any_eddsa_type(der: &key::PrivateKey) -> Result<Box<dyn SigningKey>, ()> {
    if let Ok(ed25519) = Ed25519SigningKey::new(der, SignatureScheme::ED25519) {
        return Ok(Box::new(ed25519));
    }

    // TODO: Add support for Ed448

    Err(())
}

/// A `SigningKey` for RSA-PKCS1 or RSA-PSS
pub struct RSASigningKey {
    key: Arc<RsaKeyPair>,
}

static ALL_RSA_SCHEMES: &[SignatureScheme] = &[
    SignatureScheme::RSA_PSS_SHA512,
    SignatureScheme::RSA_PSS_SHA384,
    SignatureScheme::RSA_PSS_SHA256,
    SignatureScheme::RSA_PKCS1_SHA512,
    SignatureScheme::RSA_PKCS1_SHA384,
    SignatureScheme::RSA_PKCS1_SHA256,
];

impl RSASigningKey {
    /// Make a new `RSASigningKey` from a DER encoding, in either
    /// PKCS#1 or PKCS#8 format.
    pub fn new(der: &key::PrivateKey) -> Result<RSASigningKey, ()> {
        RsaKeyPair::from_der(&der.0)
            .or_else(|_| RsaKeyPair::from_pkcs8(&der.0))
            .map(|s| RSASigningKey { key: Arc::new(s) })
            .map_err(|_| ())
    }
}

impl SigningKey for RSASigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        ALL_RSA_SCHEMES
            .iter()
            .filter(|scheme| offered.contains(scheme))
            .nth(0)
            .map(|scheme| RSASigner::new(self.key.clone(), *scheme))
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        SignatureAlgorithm::RSA
    }
}

struct RSASigner {
    key: Arc<RsaKeyPair>,
    scheme: SignatureScheme,
    encoding: &'static dyn signature::RsaEncoding,
}

impl RSASigner {
    fn new(key: Arc<RsaKeyPair>, scheme: SignatureScheme) -> Box<dyn Signer> {
        let encoding: &dyn signature::RsaEncoding = match scheme {
            SignatureScheme::RSA_PKCS1_SHA256 => &signature::RSA_PKCS1_SHA256,
            SignatureScheme::RSA_PKCS1_SHA384 => &signature::RSA_PKCS1_SHA384,
            SignatureScheme::RSA_PKCS1_SHA512 => &signature::RSA_PKCS1_SHA512,
            SignatureScheme::RSA_PSS_SHA256 => &signature::RSA_PSS_SHA256,
            SignatureScheme::RSA_PSS_SHA384 => &signature::RSA_PSS_SHA384,
            SignatureScheme::RSA_PSS_SHA512 => &signature::RSA_PSS_SHA512,
            _ => unreachable!(),
        };

        Box::new(RSASigner {
            key,
            scheme,
            encoding,
        })
    }
}

impl Signer for RSASigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let mut sig = vec![0; self.key.public_modulus_len()];

        let rng = ring::rand::SystemRandom::new();
        self.key
            .sign(self.encoding, &rng, message, &mut sig)
            .map(|_| sig)
            .map_err(|_| TLSError::General("signing failed".to_string()))
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

/// A SigningKey that uses exactly one TLS-level SignatureScheme
/// and one ring-level signature::SigningAlgorithm.
///
/// Compare this to RSASigningKey, which for a particular key is
/// willing to sign with several algorithms.  This is quite poor
/// cryptography practice, but is necessary because a given RSA key
/// is expected to work in TLS1.2 (PKCS#1 signatures) and TLS1.3
/// (PSS signatures) -- nobody is willing to obtain certificates for
/// different protocol versions.
///
/// Currently this is only implemented for ECDSA keys.
struct ECDSASigningKey {
    key: Arc<EcdsaKeyPair>,
    scheme: SignatureScheme,
}

impl ECDSASigningKey {
    /// Make a new `ECDSASigningKey` from a DER encoding in PKCS#8 format,
    /// expecting a key usable with precisely the given signature scheme.
    pub fn new(
        der: &key::PrivateKey,
        scheme: SignatureScheme,
        sigalg: &'static signature::EcdsaSigningAlgorithm,
    ) -> Result<ECDSASigningKey, ()> {
        EcdsaKeyPair::from_pkcs8(sigalg, &der.0)
            .map(|kp| ECDSASigningKey {
                key: Arc::new(kp),
                scheme,
            })
            .map_err(|_| ())
    }
}

impl SigningKey for ECDSASigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(ECDSASigner {
                key: self.key.clone(),
                scheme: self.scheme,
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

struct ECDSASigner {
    key: Arc<EcdsaKeyPair>,
    scheme: SignatureScheme,
}

impl Signer for ECDSASigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let rng = ring::rand::SystemRandom::new();
        self.key
            .sign(&rng, message)
            .map_err(|_| TLSError::General("signing failed".into()))
            .map(|sig| sig.as_ref().into())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

/// A SigningKey that uses exactly one TLS-level SignatureScheme
/// and one ring-level signature::SigningAlgorithm.
///
/// Compare this to RSASigningKey, which for a particular key is
/// willing to sign with several algorithms.  This is quite poor
/// cryptography practice, but is necessary because a given RSA key
/// is expected to work in TLS1.2 (PKCS#1 signatures) and TLS1.3
/// (PSS signatures) -- nobody is willing to obtain certificates for
/// different protocol versions.
///
/// Currently this is only implemented for Ed25519 keys.
struct Ed25519SigningKey {
    key: Arc<Ed25519KeyPair>,
    scheme: SignatureScheme,
}

impl Ed25519SigningKey {
    /// Make a new `Ed25519SigningKey` from a DER encoding in PKCS#8 format,
    /// expecting a key usable with precisely the given signature scheme.
    pub fn new(der: &key::PrivateKey, scheme: SignatureScheme) -> Result<Ed25519SigningKey, ()> {
        Ed25519KeyPair::from_pkcs8_maybe_unchecked(&der.0)
            .map(|kp| Ed25519SigningKey {
                key: Arc::new(kp),
                scheme,
            })
            .map_err(|_| ())
    }
}

impl SigningKey for Ed25519SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Ed25519Signer {
                key: self.key.clone(),
                scheme: self.scheme,
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

struct Ed25519Signer {
    key: Arc<Ed25519KeyPair>,
    scheme: SignatureScheme,
}

impl Signer for Ed25519Signer {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        Ok(self.key.sign(message).as_ref().into())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Dilithium2SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Dilithium2Signer {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Dilithium2SigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Dilithium2).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::DILITHIUM2
        }).map_err(|_| ())
    }
}

impl SigningKey for Dilithium2SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Dilithium2Signer {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Dilithium2
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Dilithium2Signer {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Dilithium3SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Dilithium3Signer {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Dilithium3SigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Dilithium3).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::DILITHIUM3
        }).map_err(|_| ())
    }
}

impl SigningKey for Dilithium3SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Dilithium3Signer {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Dilithium3
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Dilithium3Signer {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Dilithium5SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Dilithium5Signer {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Dilithium5SigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Dilithium5).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::DILITHIUM5
        }).map_err(|_| ())
    }
}

impl SigningKey for Dilithium5SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Dilithium5Signer {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Dilithium5
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Dilithium5Signer {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Falcon512SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Falcon512Signer {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Falcon512SigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Falcon512).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::FALCON512
        }).map_err(|_| ())
    }
}

impl SigningKey for Falcon512SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Falcon512Signer {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Falcon512
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Falcon512Signer {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Falcon1024SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Falcon1024Signer {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Falcon1024SigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Falcon1024).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::FALCON1024
        }).map_err(|_| ())
    }
}

impl SigningKey for Falcon1024SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Falcon1024Signer {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Falcon1024
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Falcon1024Signer {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowIClassicSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowIClassicSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowIClassicSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowIClassic).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_I_CLASSIC
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowIClassicSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowIClassicSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowIClassic
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowIClassicSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowICircumzenithalSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowICircumzenithalSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowICircumzenithalSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowICircumzenithal).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_I_CIRCUMZENITHAL
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowICircumzenithalSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowICircumzenithalSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowICircumzenithal
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowICircumzenithalSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowICompressedSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowICompressedSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowICompressedSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowICompressed).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_I_COMPRESSED
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowICompressedSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowICompressedSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowICompressed
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowICompressedSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowIiiClassicSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowIiiClassicSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowIiiClassicSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowIiiClassic).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_III_CLASSIC
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowIiiClassicSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowIiiClassicSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowIiiClassic
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowIiiClassicSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowIiiCircumzenithalSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowIiiCircumzenithalSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowIiiCircumzenithalSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowIiiCircumzenithal).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_III_CIRCUMZENITHAL
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowIiiCircumzenithalSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowIiiCircumzenithalSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowIiiCircumzenithal
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowIiiCircumzenithalSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowIiiCompressedSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowIiiCompressedSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowIiiCompressedSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowIiiCompressed).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_III_COMPRESSED
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowIiiCompressedSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowIiiCompressedSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowIiiCompressed
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowIiiCompressedSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowVClassicSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowVClassicSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowVClassicSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowVClassic).map(|key_pair| {
            Self {
                key: Arc::new(key_pair),
                scheme: SignatureScheme::RAINBOW_V_CLASSIC
            }
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowVClassicSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowVClassicSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowVClassic
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowVClassicSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowVCircumzenithalSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowVCircumzenithalSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowVCircumzenithalSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowVCircumzenithal).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_V_CIRCUMZENITHAL
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowVCircumzenithalSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowVCircumzenithalSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowVCircumzenithal
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowVCircumzenithalSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct RainbowVCompressedSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct RainbowVCompressedSigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl RainbowVCompressedSigningKey {
    /// Make a new `Dilithium2SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::RainbowVCompressed).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINBOW_V_COMPRESSED
        }).map_err(|_| ())
    }
}

impl SigningKey for RainbowVCompressedSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(RainbowVCompressedSigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::RainbowVCompressed
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for RainbowVCompressedSigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct PicnicL1FsSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct PicnicL1FsSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl PicnicL1FsSigningKey {
    /// Make a new `PicnicL1FsSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::PicnicL1Fs).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::PICNIC_L1_FS
        }).map_err(|_| ())
    }
}

impl SigningKey for PicnicL1FsSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(PicnicL1FsSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::PicnicL1Fs
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for PicnicL1FsSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct PicnicL1UrSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct PicnicL1UrSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl PicnicL1UrSigningKey {
    /// Make a new `PicnicL1UrSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::PicnicL1Ur).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::PICNIC_L1_UR
        }).map_err(|_| ())
    }
}

impl SigningKey for PicnicL1UrSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(PicnicL1UrSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::PicnicL1Ur
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for PicnicL1UrSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct PicnicL1FullSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct PicnicL1FullSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl PicnicL1FullSigningKey {
    /// Make a new `PicnicL1FullSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::PicnicL1Full).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::PICNIC_L1_FULL
        }).map_err(|_| ())
    }
}

impl SigningKey for PicnicL1FullSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(PicnicL1FullSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::PicnicL1Full
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for PicnicL1FullSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Picnic3L1SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Picnic3L1SigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Picnic3L1SigningKey {
    /// Make a new `Picnic3L1SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Picnic3L1).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::PICNIC3_L1
        }).map_err(|_| ())
    }
}

impl SigningKey for Picnic3L1SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Picnic3L1SigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Picnic3L1
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Picnic3L1SigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Picnic3L3SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Picnic3L3SigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Picnic3L3SigningKey {
    /// Make a new `Picnic3L3SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Picnic3L3).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::PICNIC3_L3
        }).map_err(|_| ())
    }
}

impl SigningKey for Picnic3L3SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Picnic3L3SigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Picnic3L3
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Picnic3L3SigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Picnic3L5SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Picnic3L5SigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Picnic3L5SigningKey {
    /// Make a new `Picnic3L5SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Picnic3L5).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::PICNIC3_L5
        }).map_err(|_| ())
    }
}

impl SigningKey for Picnic3L5SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Picnic3L5SigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Picnic3L5
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Picnic3L5SigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Rainier3L1SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Rainier3L1SigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Rainier3L1SigningKey {
    /// Make a new `Picnic3L5SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Rainier3L1).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINIER3_L1
        }).map_err(|_| ())
    }
}

impl SigningKey for Rainier3L1SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Rainier3L1SigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Rainier3L1
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Rainier3L1SigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct Rainier4L1SigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct Rainier4L1SigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl Rainier4L1SigningKey {
    /// Make a new `Picnic3L5SigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::Rainier4L1).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::RAINIER4_L1
        }).map_err(|_| ())
    }
}

impl SigningKey for Rainier4L1SigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(Rainier4L1SigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::Rainier4L1
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for Rainier4L1SigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct SphincsSha256128fRobustSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct SphincsSha256128fRobustSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl SphincsSha256128fRobustSigningKey {
    /// Make a new `SphincsSha256128fRobustSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::SphincsSha256128fRobust).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::SPHINCS_SHA_256_128F_ROBUST
        }).map_err(|_| ())
    }
}

impl SigningKey for SphincsSha256128fRobustSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(SphincsSha256128fRobustSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::SphincsSha256128fRobust
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for SphincsSha256128fRobustSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct SphincsSha256128fSimpleSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct SphincsSha256128fSimpleSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl SphincsSha256128fSimpleSigningKey {
    /// Make a new `SphincsSha256128fSimpleSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::SphincsSha256128fSimple).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::SPHINCS_SHA_256_128F_SIMPLE
        }).map_err(|_| ())
    }
}

impl SigningKey for SphincsSha256128fSimpleSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(SphincsSha256128fSimpleSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::SphincsSha256128fSimple
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for SphincsSha256128fSimpleSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct SphincsSha256128sRobustSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct SphincsSha256128sRobustSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl SphincsSha256128sRobustSigningKey {
    /// Make a new `SphincsSha256128sRobustSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::SphincsSha256128sRobust).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::SPHINCS_SHA_256_128S_ROBUST
        }).map_err(|_| ())
    }
}

impl SigningKey for SphincsSha256128sRobustSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(SphincsSha256128sRobustSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::SphincsSha256128sRobust
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for SphincsSha256128sRobustSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

//-----------------------------------------------------------------------------

struct SphincsSha256128sSimpleSigningKey {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
}

struct SphincsSha256128sSimpleSigningKeySigner {
    key: Arc<PqcKeyPair>,
    scheme: SignatureScheme,
    sigalg: sig::Algorithm,
}

impl SphincsSha256128sSimpleSigningKey {
    /// Make a new `SphincsSha256128sSimpleSigningKey`
    fn new(priv_key: &key::PrivateKey) -> Result<Self, ()> {
        PqcKeyPair::from_pkcs8(priv_key.0.as_ref(), sig::Algorithm::SphincsSha256128sSimple).map(|key_pair| Self {
            key: Arc::new(key_pair),
            scheme: SignatureScheme::SPHINCS_SHA_256_128S_SIMPLE
        }).map_err(|_| ())
    }
}

impl SigningKey for SphincsSha256128sSimpleSigningKey {
    fn choose_scheme(&self, offered: &[SignatureScheme]) -> Option<Box<dyn Signer>> {
        if offered.contains(&self.scheme) {
            Some(Box::new(SphincsSha256128sSimpleSigningKeySigner {
                key: Arc::clone(&self.key),
                scheme: self.scheme,
                sigalg: sig::Algorithm::SphincsSha256128sSimple
            }))
        } else {
            None
        }
    }

    fn algorithm(&self) -> SignatureAlgorithm {
        use crate::msgs::handshake::DecomposedSignatureScheme;
        self.scheme.sign()
    }
}

impl Signer for SphincsSha256128sSimpleSigningKeySigner {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, TLSError> {
        let signature = self.key.sign(message, self.sigalg).map_err(|_| TLSError::General("Could not sign the given message!".to_string()))?;
        Ok(signature.into_vec())
    }

    fn get_scheme(&self) -> SignatureScheme {
        self.scheme
    }
}

/// The set of schemes we support for signatures and
/// that are allowed for TLS1.3.
pub fn supported_sign_tls13() -> &'static [SignatureScheme] {
    &[
        SignatureScheme::ECDSA_NISTP384_SHA384,
        SignatureScheme::ECDSA_NISTP256_SHA256,
        SignatureScheme::RSA_PSS_SHA512,
        SignatureScheme::RSA_PSS_SHA384,
        SignatureScheme::RSA_PSS_SHA256,
        SignatureScheme::ED25519,
        SignatureScheme::DILITHIUM2,
        SignatureScheme::DILITHIUM3,
        SignatureScheme::DILITHIUM5,
        SignatureScheme::FALCON512,
        SignatureScheme::FALCON1024,
        SignatureScheme::RAINBOW_I_CLASSIC,
        SignatureScheme::RAINBOW_I_CIRCUMZENITHAL,
        SignatureScheme::RAINBOW_I_COMPRESSED,
        SignatureScheme::RAINBOW_III_CLASSIC,
        SignatureScheme::RAINBOW_III_CIRCUMZENITHAL,
        SignatureScheme::RAINBOW_III_COMPRESSED,
        SignatureScheme::RAINBOW_V_CLASSIC,
        SignatureScheme::RAINBOW_V_CIRCUMZENITHAL,
        SignatureScheme::RAINBOW_V_COMPRESSED,
        SignatureScheme::PICNIC3_L1,
        SignatureScheme::PICNIC3_L3,
        SignatureScheme::PICNIC3_L5,
        SignatureScheme::PICNIC_L1_FS,
        SignatureScheme::PICNIC_L1_UR,
        SignatureScheme::PICNIC_L1_FULL,
        SignatureScheme::RAINIER3_L1,    
        SignatureScheme::RAINIER4_L1,
        SignatureScheme::SPHINCS_SHA_256_128F_ROBUST,
        SignatureScheme::SPHINCS_SHA_256_128F_SIMPLE,
        SignatureScheme::SPHINCS_SHA_256_128S_ROBUST,
        SignatureScheme::SPHINCS_SHA_256_128S_SIMPLE,
    ]
}
