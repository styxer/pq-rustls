use crate::cipher;
use crate::msgs::codec::{Codec, Reader};
use crate::msgs::enums::{CipherSuite, HashAlgorithm, SignatureAlgorithm, SignatureScheme};
use crate::msgs::enums::{NamedGroup, ProtocolVersion};
use crate::msgs::handshake::DecomposedSignatureScheme;
use crate::msgs::handshake::KeyExchangeAlgorithm;
use crate::msgs::handshake::{ClientECDHParams, ServerECDHParams};

use ring;
use std::fmt;

use oqs::*;

/// Bulk symmetric encryption scheme used by a cipher suite.
#[allow(non_camel_case_types)]
#[derive(Debug, PartialEq)]
pub enum BulkAlgorithm {
    /// AES with 128-bit keys in Galois counter mode.
    AES_128_GCM,

    /// AES with 256-bit keys in Galois counter mode.
    AES_256_GCM,

    /// Chacha20 for confidentiality with poly1305 for authenticity.
    CHACHA20_POLY1305,
}

/// The result of a key exchange.  This has our public key,
/// and the agreed shared secret (also known as the "premaster secret"
/// in TLS1.0-era protocols, and "Z" in TLS1.3).
pub struct KeyExchangeResult {
    pub pubkey: Vec<u8>,
    pub shared_secret: Vec<u8>,
}

enum KeyExchangePrivateKey {
    PQCKey {
        privkey: kem::SecretKey
    },
    NormalKey {
        privkey: ring::agreement::EphemeralPrivateKey
    }
}

impl KeyExchangePrivateKey {
    pub(crate) fn normal_key(self) -> ring::agreement::EphemeralPrivateKey {
        match self {
            KeyExchangePrivateKey::NormalKey{privkey} => {
                privkey
            },
            _ => panic!("This method MUST NOT not be used with PQC secret keys!")
        }
    }

    pub(crate) fn pqc_key(self) -> kem::SecretKey {
        match self {
            KeyExchangePrivateKey::PQCKey{privkey} => {
                privkey
            },
            _ => panic!("This method MUST NOT not be used with non PQC private keys!")
        }
    }
}

/// An in-progress key exchange.  This has the algorithm,
/// our private key, and our public key.
pub struct KeyExchange {
    skxg: &'static SupportedKxGroup,
    privkey: KeyExchangePrivateKey,
    pub pubkey: Vec<u8>,
}

impl KeyExchange {
    pub(crate) fn enable_pqc(name: NamedGroup) -> bool {
        match name {
            NamedGroup::SABER => true,
            NamedGroup::LIGHT_SABER => true,
            NamedGroup::FIRE_SABER => true,
            NamedGroup::KYBER512 => true,
            NamedGroup::KYBER768 => true,
            NamedGroup::KYBER1024 => true,
            NamedGroup::KYBER512_90S => true,
            NamedGroup::KYBER768_90S => true,
            NamedGroup::KYBER1024_90S => true,
            NamedGroup::CLASSIC_MC_ELIECE_348864 => true,
            NamedGroup::CLASSIC_MC_ELIECE_348864F => true,
            NamedGroup::CLASSIC_MC_ELIECE_460896 => true,
            NamedGroup::CLASSIC_MC_ELIECE_460896F => true,
            NamedGroup::CLASSIC_MC_ELIECE_6688128 => true,
            NamedGroup::CLASSIC_MC_ELIECE_6688128F => true,
            NamedGroup::CLASSIC_MC_ELIECE_6960119 => true,
            NamedGroup::CLASSIC_MC_ELIECE_6960119F => true,
            NamedGroup::CLASSIC_MC_ELIECE_8192128 => true,
            NamedGroup::CLASSIC_MC_ELIECE_8192128F => true,
            NamedGroup::NTRU_HPS_2048509 => true,
            NamedGroup::NTRU_HPS_2048677 => true,
            NamedGroup::NTRU_HPS_4096821 => true,
            NamedGroup::NTRU_HRSS_701 => true,
            NamedGroup::NTRU_PRIME_NTRU_LPR_653 => true,
            NamedGroup::NTRU_PRIME_NTRU_LPR_761 => true,
            NamedGroup::NTRU_PRIME_NTRU_LPR_857 => true,
            NamedGroup::NTRU_PRIME_S_NTRU_P_653 => true,
            NamedGroup::NTRU_PRIME_S_NTRU_P_761 => true,
            NamedGroup::NTRU_PRIME_S_NTRU_P_857 => true,
            _ => false
        }
    }

    /// Choose a SupportedKxGroup by name, from a list of supported groups.
    pub(crate) fn choose(
        name: NamedGroup
    ) -> Option<&'static SupportedKxGroup> {
        ALL_KX_GROUPS
            .iter()
            .find(|skxg| skxg.name == name)
            .cloned()
    }

    fn create_default(skxg: &'static SupportedKxGroup) -> Option<Self> {
        let (privkey, pubkey) = skxg.agreement_algorithm.create_default().ok()?;

        Some(Self {
            skxg,
            privkey: KeyExchangePrivateKey::NormalKey{privkey},
            pubkey: Vec::from(pubkey.as_ref()),
        })
    }

    fn create_pqc(skxg: &'static SupportedKxGroup) -> Option<Self> {
        let (kem_pk, kem_sk) = skxg.agreement_algorithm.create_pqc().ok()?;
        Some(Self {
            skxg,
            privkey: KeyExchangePrivateKey::PQCKey{privkey: kem_sk},
            pubkey: kem_pk.into_vec(),
        })
    }

    /// Start a key exchange, using the given SupportedKxGroup.
    ///
    /// This generates an ephemeral key pair and stores it in the returned KeyExchange object.
    pub(crate) fn start(skxg: &'static SupportedKxGroup) -> Option<Self> {
        let enable_pqc = Self::enable_pqc(skxg.name);
        if !enable_pqc {
            Self::create_default(skxg)
        }
        else {
            Self::create_pqc(skxg)
        }
    }

    /// Return the group being used.
    pub(crate) fn group(&self) -> NamedGroup {
        self.skxg.name
    }

    fn complete_default(self, peer: &[u8]) -> Option<KeyExchangeResult> {
        self.skxg.agreement_algorithm.agree_ephemeral(self.pubkey, self.privkey.normal_key(), peer).ok()
    }

    pub(crate) fn decapsulate(self, peer: &[u8]) -> Option<KeyExchangeResult> {
        let shared_secret = self.skxg.agreement_algorithm.decapsulate(&self.privkey.pqc_key(), peer).ok()?;

        Some(KeyExchangeResult{
            pubkey: self.pubkey,
            shared_secret: shared_secret.into_vec()
        })
    }

    pub(crate) fn encapsulate(self, kem_pk_bytes: &Vec<u8>) -> Option<KeyExchangeResult> {
        let (kem_ct, kem_ss) = self.skxg.agreement_algorithm.encapsulate(kem_pk_bytes).ok()?;

        Some(KeyExchangeResult {
            pubkey: kem_ct.into_vec(),
            shared_secret: kem_ss.into_vec(),
        })
    }

    /// Completes the key exchange, given the peer's public key.  The shared
    /// secret is returned as a KeyExchangeResult.
    pub(crate) fn complete(self, peer: &[u8]) -> Option<KeyExchangeResult> {
        let enable_pqc = Self::enable_pqc(self.skxg.name);

        if !enable_pqc {
            self.complete_default(peer)
        }
        else {
            self.decapsulate(peer)
        }
    }

    pub(crate) fn supported_groups() -> Vec<NamedGroup> {
        let mut named_groups = Vec::new();

        for kx_group in ALL_KX_GROUPS {
            named_groups.push(kx_group.name);
        }

        return named_groups;
    }

    // ---------------TLS1.2 compatibility---------------

    pub fn check_client_params(&self, kx_params: &[u8]) -> bool {
        self.decode_client_params(kx_params)
            .is_some()
    }

    fn decode_client_params(&self, kx_params: &[u8]) -> Option<ClientECDHParams> {
        let mut rd = Reader::init(kx_params);
        let ecdh_params = ClientECDHParams::read(&mut rd).unwrap();
        if rd.any_left() {
            None
        } else {
            Some(ecdh_params)
        }
    }

    pub fn server_complete(self, kx_params: &[u8]) -> Option<KeyExchangeResult> {
        self.decode_client_params(kx_params)
            .and_then(|ecdh| self.complete(&ecdh.public.0))
    }

    // ---------------TLS1.2 compatibility---------------

    pub fn start_ecdhe(named_group: NamedGroup) -> Option<KeyExchange> {
        KeyExchange::start(KeyExchange::choose(named_group)?)
    }

    pub fn client_ecdhe(kx_params: &[u8]) -> Option<KeyExchangeResult> {
        let mut rd = Reader::init(kx_params);
        let ecdh_params = ServerECDHParams::read(&mut rd)?;

        KeyExchange::start_ecdhe(ecdh_params.curve_params.named_group)?
            .complete(&ecdh_params.public.0)
    }
}

#[derive(Debug)]
enum KxGroupAlgorithm {
    PQCAlgorithm {
        algorithm: &'static kem::Algorithm
    },
    NormalAlgorithm {
        algorithm: &'static ring::agreement::Algorithm
    }
}

impl KxGroupAlgorithm {
    pub(crate) fn encapsulate(&self, kem_pk_bytes: &Vec<u8>) -> Result<(oqs::kem::Ciphertext, oqs::kem::SharedSecret)>{
        match self {
            KxGroupAlgorithm::PQCAlgorithm{algorithm} => {
                let kemalg = kem::Kem::new(**algorithm)?;
                let kem_pk = kemalg.public_key_from_bytes(kem_pk_bytes.as_ref()).ok_or(Error::Error)?;

                kemalg.encapsulate(&kem_pk)
            },
            _ => panic!("This method MUST NOT not be used with non PQC algorithms!")
        }
    }

    pub(crate) fn decapsulate(&self, private_key: &oqs::kem::SecretKey, peer: &[u8]) -> Result<oqs::kem::SharedSecret> {
        match self {
            KxGroupAlgorithm::PQCAlgorithm{algorithm} => {
                let kemalg     = kem::Kem::new(**algorithm)?;
                let ciphertext = kemalg.ciphertext_from_bytes(peer).ok_or(Error::Error)?;

                kemalg.decapsulate(private_key, ciphertext)
            },
            _ => panic!("This method MUST NOT not be used with non PQC algorithms!")
        }
    }

    pub(crate) fn agree_ephemeral(&self, pubkey: Vec<u8>, privkey: ring::agreement::EphemeralPrivateKey, peer: &[u8]) -> Result<KeyExchangeResult> {
        match self {
            KxGroupAlgorithm::NormalAlgorithm{algorithm} => {
                let peer_key = ring::agreement::UnparsedPublicKey::new(algorithm, peer);
                ring::agreement::agree_ephemeral(privkey, &peer_key, Error::Error, move |v| {
                    Ok(KeyExchangeResult {
                        pubkey,
                        shared_secret: Vec::from(v),
                    })
                })
            },
            _ => panic!("This method MUST NOT not be used with PQC algorithms!")
        }
    }

    pub(crate) fn create_default(&self) -> Result<(ring::agreement::EphemeralPrivateKey, ring::agreement::PublicKey)> {
        match self {
            KxGroupAlgorithm::NormalAlgorithm{algorithm} => {
                let rng     = ring::rand::SystemRandom::new();
                let privkey = ring::agreement::EphemeralPrivateKey::generate(algorithm, &rng).ok().ok_or(Error::Error)?;

                let pubkey = privkey.compute_public_key().ok().ok_or(Error::Error)?;

                Ok((privkey, pubkey))
            },
            _ => panic!("This method MUST NOT not be used with PQC algorithms!")
        }
    }

    pub(crate) fn create_pqc(&self) -> Result<(oqs::kem::PublicKey, oqs::kem::SecretKey)> {
        match self {
            KxGroupAlgorithm::PQCAlgorithm{algorithm} => {
                let kemalg = kem::Kem::new(**algorithm).ok().ok_or(Error::Error)?;
                kemalg.keypair()
            },
            _ => panic!("This method MUST NOT not be used with non PQC algorithms!")
        }
    }
}

/// A key-exchange group supported by rustls.
///
/// All possible instances of this class are provided by the library in
/// the `ALL_KX_GROUPS` array.
#[derive(Debug)]
pub struct SupportedKxGroup {
    /// The IANA "TLS Supported Groups" name of the group
    pub name: NamedGroup,

    /// The corresponding key-exchange/agreement algorithm
    agreement_algorithm: KxGroupAlgorithm
}

/// Ephemeral ECDH on curve25519 (see RFC7748)
pub static X25519: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::X25519,
    agreement_algorithm: KxGroupAlgorithm::NormalAlgorithm{algorithm: &ring::agreement::X25519}
};

/// Ephemeral ECDH on secp256r1 (aka NIST-P256)
pub static SECP256R1: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::secp256r1,
    agreement_algorithm: KxGroupAlgorithm::NormalAlgorithm{algorithm: &ring::agreement::ECDH_P256}
};

/// Ephemeral ECDH on secp384r1 (aka NIST-P384)
pub static SECP384R1: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::secp384r1,
    agreement_algorithm: KxGroupAlgorithm::NormalAlgorithm{algorithm: &ring::agreement::ECDH_P384}
};

/// Lattice based PQC Algorithm
pub static SABER: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::SABER,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Saber}
};

/// Lattice based PQC Algorithm
pub static LIGHT_SABER: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::LIGHT_SABER,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Lightsaber}
};

/// Lattice based PQC Algorithm
pub static FIRE_SABER: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::FIRE_SABER,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Firesaber}
};

/// Lattice based PQC Algorithm
pub static KYBER512: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::KYBER512,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Kyber512}
};

/// Lattice based PQC Algorithm
pub static KYBER768: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::KYBER768,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Kyber768}
};

/// Lattice based PQC Algorithm
pub static KYBER1024: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::KYBER1024,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Kyber1024}
};

/// Lattice based PQC Algorithm
pub static KYBER512_90S: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::KYBER512_90S,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Kyber512_90s}
};

/// Lattice based PQC Algorithm
pub static KYBER768_90S: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::KYBER768_90S,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Kyber768_90s}
};

/// Lattice based PQC Algorithm
pub static KYBER1024_90S: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::KYBER1024_90S,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::Kyber1024_90s}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_348864: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_348864,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece348864}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_348864F: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_348864F,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece348864f}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_460896: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_460896,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece460896}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_460896F: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_460896F,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece460896f}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_6688128: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_6688128,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece6688128}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_6688128F: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_6688128F,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece6688128f}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_6960119: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_6960119,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece6960119}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_6960119F: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_6960119F,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece6960119f}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_8192128: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_8192128,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece8192128}
};

/// Goppa code based PQC Algorithm
pub static CLASSIC_MC_ELIECE_8192128F: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::CLASSIC_MC_ELIECE_8192128F,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::ClassicMcEliece8192128f}
};

/// lattice based PQC Algorithm
pub static NTRU_HPS_2048509: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_HPS_2048509,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruHps2048509}
};

/// lattice based PQC Algorithm
pub static NTRU_HPS_2048677: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_HPS_2048677,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruHps2048677}
};

/// lattice based PQC Algorithm
pub static NTRU_HPS_4096821: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_HPS_4096821,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruHps4096821}
};

/// lattice based PQC Algorithm
pub static NTRU_HRSS_701: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_HRSS_701,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruHrss701}
};

/// lattice based PQC Algorithm
pub static NTRU_PRIME_NTRU_LPR_653: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_PRIME_NTRU_LPR_653,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruPrimeNtrulpr653}
};

/// lattice based PQC Algorithm
pub static NTRU_PRIME_NTRU_LPR_761: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_PRIME_NTRU_LPR_761,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruPrimeNtrulpr761}
};

/// lattice based PQC Algorithm
pub static NTRU_PRIME_NTRU_LPR_857: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_PRIME_NTRU_LPR_857,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruPrimeNtrulpr857}
};

/// lattice based PQC Algorithm
pub static NTRU_PRIME_S_NTRU_P_653: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_PRIME_S_NTRU_P_653,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruPrimeSntrup653}
};

/// lattice based PQC Algorithm
pub static NTRU_PRIME_S_NTRU_P_761: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_PRIME_S_NTRU_P_761,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruPrimeSntrup761}
};

/// lattice based PQC Algorithm
pub static NTRU_PRIME_S_NTRU_P_857: SupportedKxGroup = SupportedKxGroup {
    name: NamedGroup::NTRU_PRIME_S_NTRU_P_857,
    agreement_algorithm: KxGroupAlgorithm::PQCAlgorithm{algorithm: &kem::Algorithm::NtruPrimeSntrup857}
};

/// A list of all the key exchange groups supported by rustls.
pub static ALL_KX_GROUPS: [&SupportedKxGroup; 32] = [&KYBER512,
                                                     &KYBER768,
                                                     &KYBER1024,
                                                     &KYBER512_90S,
                                                     &KYBER768_90S,
                                                     &KYBER1024_90S,
                                                     &CLASSIC_MC_ELIECE_348864,
                                                     &CLASSIC_MC_ELIECE_348864F,
                                                     &CLASSIC_MC_ELIECE_460896,
                                                     &CLASSIC_MC_ELIECE_460896F,
                                                     &CLASSIC_MC_ELIECE_6688128,
                                                     &CLASSIC_MC_ELIECE_6688128F,
                                                     &CLASSIC_MC_ELIECE_6960119,
                                                     &CLASSIC_MC_ELIECE_6960119F,
                                                     &CLASSIC_MC_ELIECE_8192128,
                                                     &CLASSIC_MC_ELIECE_8192128F,
                                                     &SABER,
                                                     &LIGHT_SABER,
                                                     &FIRE_SABER,
                                                     &NTRU_HPS_2048509,
                                                     &NTRU_HPS_2048677,
                                                     &NTRU_HPS_4096821,
                                                     &NTRU_HRSS_701,
                                                     &NTRU_PRIME_NTRU_LPR_653,
                                                     &NTRU_PRIME_NTRU_LPR_761,
                                                     &NTRU_PRIME_NTRU_LPR_857,
                                                     &NTRU_PRIME_S_NTRU_P_653,
                                                     &NTRU_PRIME_S_NTRU_P_761,
                                                     &NTRU_PRIME_S_NTRU_P_857,
                                                     &X25519,
                                                     &SECP256R1,
                                                     &SECP384R1];

/// A list of all non-pqc key exchange groups supported by rustls.
pub static NO_PQC_KX_GROUPS: [&SupportedKxGroup; 3] = [&X25519, &SECP256R1, &SECP384R1];

/// A list of all the PQC key exchange groups supported by rustls.
pub static PQC_KX_GROUPS: [&SupportedKxGroup; 29] = [&KYBER512,
                                                     &KYBER768,
                                                     &KYBER1024,
                                                     &KYBER512_90S,
                                                     &KYBER768_90S,
                                                     &KYBER1024_90S,
                                                     &CLASSIC_MC_ELIECE_348864,
                                                     &CLASSIC_MC_ELIECE_348864F,
                                                     &CLASSIC_MC_ELIECE_460896,
                                                     &CLASSIC_MC_ELIECE_460896F,
                                                     &CLASSIC_MC_ELIECE_6688128,
                                                     &CLASSIC_MC_ELIECE_6688128F,
                                                     &CLASSIC_MC_ELIECE_6960119,
                                                     &CLASSIC_MC_ELIECE_6960119F,
                                                     &CLASSIC_MC_ELIECE_8192128,
                                                     &CLASSIC_MC_ELIECE_8192128F,
                                                     &SABER,
                                                     &LIGHT_SABER,
                                                     &FIRE_SABER,
                                                     &NTRU_HPS_2048509,
                                                     &NTRU_HPS_2048677,
                                                     &NTRU_HPS_4096821,
                                                     &NTRU_HRSS_701,
                                                     &NTRU_PRIME_NTRU_LPR_653,
                                                     &NTRU_PRIME_NTRU_LPR_761,
                                                     &NTRU_PRIME_NTRU_LPR_857,
                                                     &NTRU_PRIME_S_NTRU_P_653,
                                                     &NTRU_PRIME_S_NTRU_P_761,
                                                     &NTRU_PRIME_S_NTRU_P_857];

/// A cipher suite supported by rustls.
///
/// All possible instances of this class are provided by the library in
/// the `ALL_CIPHERSUITES` array.
pub struct SupportedCipherSuite {
    /// The TLS enumeration naming this cipher suite.
    pub suite: CipherSuite,

    /// How to exchange/agree keys.
    pub kx: KeyExchangeAlgorithm,

    /// How to do bulk encryption.
    pub bulk: BulkAlgorithm,

    /// How to do hashing.
    pub hash: HashAlgorithm,

    /// How to sign messages for authentication.
    ///
    /// This is not present for TLS1.3, because authentication is orthogonal
    /// to the ciphersuite concept there.
    pub sign: Option<&'static [SignatureScheme]>,

    /// Encryption key length, for the bulk algorithm.
    pub enc_key_len: usize,

    /// How long the fixed part of the 'IV' is.
    ///
    /// This isn't usually an IV, but we continue the
    /// terminology misuse to match the standard.
    pub fixed_iv_len: usize,

    /// This is a non-standard extension which extends the
    /// key block to provide an initial explicit nonce offset,
    /// in a deterministic and safe way.  GCM needs this,
    /// chacha20poly1305 works this way by design.
    pub explicit_nonce_len: usize,

    pub(crate) hkdf_algorithm: ring::hkdf::Algorithm,
    pub(crate) aead_algorithm: &'static ring::aead::Algorithm,
    pub(crate) build_tls12_encrypter: Option<cipher::BuildTLS12Encrypter>,
    pub(crate) build_tls12_decrypter: Option<cipher::BuildTLS12Decrypter>,
}

impl PartialEq for SupportedCipherSuite {
    fn eq(&self, other: &SupportedCipherSuite) -> bool {
        self.suite == other.suite
    }
}

impl fmt::Debug for SupportedCipherSuite {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SupportedCipherSuite")
            .field("suite", &self.suite)
            .field("kx", &self.kx)
            .field("bulk", &self.bulk)
            .field("hash", &self.hash)
            .field("sign", &self.sign)
            .field("enc_key_len", &self.enc_key_len)
            .field("fixed_iv_len", &self.fixed_iv_len)
            .field("explicit_nonce_len", &self.explicit_nonce_len)
            .finish()
    }
}

impl SupportedCipherSuite {
    /// Which hash function to use with this suite.
    pub fn get_hash(&self) -> &'static ring::digest::Algorithm {
        self.hkdf_algorithm
            .hmac_algorithm()
            .digest_algorithm()
    }

    /// We have parameters and a verified public key in `kx_params`.
    /// Generate an ephemeral key, generate the shared secret, and
    /// return it and the public half in a `KeyExchangeResult`.
    pub fn do_client_kx(&self, kx_params: &[u8]) -> Option<KeyExchangeResult> {
        println!("{:#?}", self.kx);
        match self.kx {
            KeyExchangeAlgorithm::ECDHE => KeyExchange::client_ecdhe(kx_params),
            _ => None,
        }
    }

    /// Start the KX process with the given group.  This generates
    /// the server's share, but we don't yet have the client's share.
    pub fn start_server_kx(&self, named_group: NamedGroup) -> Option<KeyExchange> {
        match self.kx {
            KeyExchangeAlgorithm::ECDHE => KeyExchange::start_ecdhe(named_group),
            _ => None,
        }
    }

    /// Resolve the set of supported `SignatureScheme`s from the
    /// offered `SupportedSignatureSchemes`.  If we return an empty
    /// set, the handshake terminates.
    pub fn resolve_sig_schemes(&self, offered: &[SignatureScheme]) -> Vec<SignatureScheme> {
        if let Some(our_preference) = self.sign {
            our_preference
                .iter()
                .filter(|pref| offered.contains(pref))
                .cloned()
                .collect()
        } else {
            vec![]
        }
    }

    /// Length of key block that needs to be output by the key
    /// derivation phase for this suite.
    pub fn key_block_len(&self) -> usize {
        (self.enc_key_len + self.fixed_iv_len) * 2 + self.explicit_nonce_len
    }

    /// Return true if this suite is usable for TLS `version`.
    pub fn usable_for_version(&self, version: ProtocolVersion) -> bool {
        match version {
            ProtocolVersion::TLSv1_3 => self.build_tls12_encrypter.is_none(),
            ProtocolVersion::TLSv1_2 => self.build_tls12_encrypter.is_some(),
            _ => false,
        }
    }

    /// Return true if this suite is usable for a key only offering `sigalg`
    /// signatures.  This resolves to true for all TLS1.3 suites.
    pub fn usable_for_sigalg(&self, sigalg: SignatureAlgorithm) -> bool {
        match self.sign {
            None => true, // no constraint expressed by ciphersuite (eg, TLS1.3)
            Some(schemes) => schemes
                .iter()
                .any(|scheme| scheme.sign() == sigalg),
        }
    }

    /// Can a session using suite self resume using suite new_suite?
    pub fn can_resume_to(&self, new_suite: &SupportedCipherSuite) -> bool {
        if self.usable_for_version(ProtocolVersion::TLSv1_3)
            && new_suite.usable_for_version(ProtocolVersion::TLSv1_3)
        {
            // TLS1.3 actually specifies requirements here: suites are compatible
            // for resumption if they have the same KDF hash
            self.hash == new_suite.hash
        } else if self.usable_for_version(ProtocolVersion::TLSv1_2)
            && new_suite.usable_for_version(ProtocolVersion::TLSv1_2)
        {
            // Previous versions don't specify any constraint, so we don't
            // resume between suites to avoid bad interactions.
            self.suite == new_suite.suite
        } else {
            // Suites for different versions definitely can't resume!
            false
        }
    }
}

static TLS12_ECDSA_SCHEMES: &[SignatureScheme] = &[
    SignatureScheme::ED25519,
    SignatureScheme::ECDSA_NISTP521_SHA512,
    SignatureScheme::ECDSA_NISTP384_SHA384,
    SignatureScheme::ECDSA_NISTP256_SHA256,
];

static TLS12_RSA_SCHEMES: &[SignatureScheme] = &[
    SignatureScheme::RSA_PSS_SHA512,
    SignatureScheme::RSA_PSS_SHA384,
    SignatureScheme::RSA_PSS_SHA256,
    SignatureScheme::RSA_PKCS1_SHA512,
    SignatureScheme::RSA_PKCS1_SHA384,
    SignatureScheme::RSA_PKCS1_SHA256,
];

/// The TLS1.2 ciphersuite TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256.
pub static TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256: SupportedCipherSuite =
    SupportedCipherSuite {
        suite: CipherSuite::TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
        kx: KeyExchangeAlgorithm::ECDHE,
        sign: Some(TLS12_ECDSA_SCHEMES),
        bulk: BulkAlgorithm::CHACHA20_POLY1305,
        hash: HashAlgorithm::SHA256,
        enc_key_len: 32,
        fixed_iv_len: 12,
        explicit_nonce_len: 0,
        hkdf_algorithm: ring::hkdf::HKDF_SHA256,
        aead_algorithm: &ring::aead::CHACHA20_POLY1305,
        build_tls12_encrypter: Some(cipher::build_tls12_chacha_encrypter),
        build_tls12_decrypter: Some(cipher::build_tls12_chacha_decrypter),
    };

/// The TLS1.2 ciphersuite TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256
pub static TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256: SupportedCipherSuite =
    SupportedCipherSuite {
        suite: CipherSuite::TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
        kx: KeyExchangeAlgorithm::ECDHE,
        sign: Some(TLS12_RSA_SCHEMES),
        bulk: BulkAlgorithm::CHACHA20_POLY1305,
        hash: HashAlgorithm::SHA256,
        enc_key_len: 32,
        fixed_iv_len: 12,
        explicit_nonce_len: 0,
        hkdf_algorithm: ring::hkdf::HKDF_SHA256,
        aead_algorithm: &ring::aead::CHACHA20_POLY1305,
        build_tls12_encrypter: Some(cipher::build_tls12_chacha_encrypter),
        build_tls12_decrypter: Some(cipher::build_tls12_chacha_decrypter),
    };

/// The TLS1.2 ciphersuite TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
pub static TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
    kx: KeyExchangeAlgorithm::ECDHE,
    sign: Some(TLS12_RSA_SCHEMES),
    bulk: BulkAlgorithm::AES_128_GCM,
    hash: HashAlgorithm::SHA256,
    enc_key_len: 16,
    fixed_iv_len: 4,
    explicit_nonce_len: 8,
    hkdf_algorithm: ring::hkdf::HKDF_SHA256,
    aead_algorithm: &ring::aead::AES_128_GCM,
    build_tls12_encrypter: Some(cipher::build_tls12_gcm_128_encrypter),
    build_tls12_decrypter: Some(cipher::build_tls12_gcm_128_decrypter),
};

/// The TLS1.2 ciphersuite TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
pub static TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
    kx: KeyExchangeAlgorithm::ECDHE,
    sign: Some(TLS12_RSA_SCHEMES),
    bulk: BulkAlgorithm::AES_256_GCM,
    hash: HashAlgorithm::SHA384,
    enc_key_len: 32,
    fixed_iv_len: 4,
    explicit_nonce_len: 8,
    hkdf_algorithm: ring::hkdf::HKDF_SHA384,
    aead_algorithm: &ring::aead::AES_256_GCM,
    build_tls12_encrypter: Some(cipher::build_tls12_gcm_256_encrypter),
    build_tls12_decrypter: Some(cipher::build_tls12_gcm_256_decrypter),
};

/// The TLS1.2 ciphersuite TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
pub static TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
    kx: KeyExchangeAlgorithm::ECDHE,
    sign: Some(TLS12_ECDSA_SCHEMES),
    bulk: BulkAlgorithm::AES_128_GCM,
    hash: HashAlgorithm::SHA256,
    enc_key_len: 16,
    fixed_iv_len: 4,
    explicit_nonce_len: 8,
    hkdf_algorithm: ring::hkdf::HKDF_SHA256,
    aead_algorithm: &ring::aead::AES_128_GCM,
    build_tls12_encrypter: Some(cipher::build_tls12_gcm_128_encrypter),
    build_tls12_decrypter: Some(cipher::build_tls12_gcm_128_decrypter),
};

/// The TLS1.2 ciphersuite TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
pub static TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
    kx: KeyExchangeAlgorithm::ECDHE,
    sign: Some(TLS12_ECDSA_SCHEMES),
    bulk: BulkAlgorithm::AES_256_GCM,
    hash: HashAlgorithm::SHA384,
    enc_key_len: 32,
    fixed_iv_len: 4,
    explicit_nonce_len: 8,
    hkdf_algorithm: ring::hkdf::HKDF_SHA384,
    aead_algorithm: &ring::aead::AES_256_GCM,
    build_tls12_encrypter: Some(cipher::build_tls12_gcm_256_encrypter),
    build_tls12_decrypter: Some(cipher::build_tls12_gcm_256_decrypter),
};

/// The TLS1.3 ciphersuite TLS_CHACHA20_POLY1305_SHA256
pub static TLS13_CHACHA20_POLY1305_SHA256: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS13_CHACHA20_POLY1305_SHA256,
    kx: KeyExchangeAlgorithm::BulkOnly,
    sign: None,
    bulk: BulkAlgorithm::CHACHA20_POLY1305,
    hash: HashAlgorithm::SHA256,
    enc_key_len: 32,
    fixed_iv_len: 12,
    explicit_nonce_len: 0,
    hkdf_algorithm: ring::hkdf::HKDF_SHA256,
    aead_algorithm: &ring::aead::CHACHA20_POLY1305,
    build_tls12_encrypter: None,
    build_tls12_decrypter: None,
};

/// The TLS1.3 ciphersuite TLS_AES_256_GCM_SHA384
pub static TLS13_AES_256_GCM_SHA384: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS13_AES_256_GCM_SHA384,
    kx: KeyExchangeAlgorithm::BulkOnly,
    sign: None,
    bulk: BulkAlgorithm::AES_256_GCM,
    hash: HashAlgorithm::SHA384,
    enc_key_len: 32,
    fixed_iv_len: 12,
    explicit_nonce_len: 0,
    hkdf_algorithm: ring::hkdf::HKDF_SHA384,
    aead_algorithm: &ring::aead::AES_256_GCM,
    build_tls12_encrypter: None,
    build_tls12_decrypter: None,
};

/// The TLS1.3 ciphersuite TLS_AES_128_GCM_SHA256
pub static TLS13_AES_128_GCM_SHA256: SupportedCipherSuite = SupportedCipherSuite {
    suite: CipherSuite::TLS13_AES_128_GCM_SHA256,
    kx: KeyExchangeAlgorithm::BulkOnly,
    sign: None,
    bulk: BulkAlgorithm::AES_128_GCM,
    hash: HashAlgorithm::SHA256,
    enc_key_len: 16,
    fixed_iv_len: 12,
    explicit_nonce_len: 0,
    hkdf_algorithm: ring::hkdf::HKDF_SHA256,
    aead_algorithm: &ring::aead::AES_128_GCM,
    build_tls12_encrypter: None,
    build_tls12_decrypter: None,
};

/// A list of all the cipher suites supported by rustls.
pub static ALL_CIPHERSUITES: [&SupportedCipherSuite; 9] = [
    // TLS1.3 suites
    &TLS13_CHACHA20_POLY1305_SHA256,
    &TLS13_AES_256_GCM_SHA384,
    &TLS13_AES_128_GCM_SHA256,
    // TLS1.2 suites
    &TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
    &TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
    &TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
    &TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
    &TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
    &TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
];

// These both O(N^2)!
pub fn choose_ciphersuite_preferring_client(
    client_suites: &[CipherSuite],
    server_suites: &[&'static SupportedCipherSuite],
) -> Option<&'static SupportedCipherSuite> {
    for client_suite in client_suites {
        if let Some(selected) = server_suites
            .iter()
            .find(|x| *client_suite == x.suite)
        {
            return Some(*selected);
        }
    }

    None
}

pub fn choose_ciphersuite_preferring_server(
    client_suites: &[CipherSuite],
    server_suites: &[&'static SupportedCipherSuite],
) -> Option<&'static SupportedCipherSuite> {
    if let Some(selected) = server_suites
        .iter()
        .find(|x| client_suites.contains(&x.suite))
    {
        return Some(*selected);
    }

    None
}

/// Return a list of the ciphersuites in `all` with the suites
/// incompatible with `SignatureAlgorithm` `sigalg` removed.
pub fn reduce_given_sigalg(
    all: &[&'static SupportedCipherSuite],
    sigalg: SignatureAlgorithm,
) -> Vec<&'static SupportedCipherSuite> {
    all.iter()
        .filter(|&&suite| suite.usable_for_sigalg(sigalg))
        .cloned()
        .collect()
}

/// Return a list of the ciphersuites in `all` with the suites
/// incompatible with the chosen `version` removed.
pub fn reduce_given_version(
    all: &[&'static SupportedCipherSuite],
    version: ProtocolVersion,
) -> Vec<&'static SupportedCipherSuite> {
    all.iter()
        .filter(|&&suite| suite.usable_for_version(version))
        .cloned()
        .collect()
}

/// Return true if `sigscheme` is usable by any of the given suites.
pub fn compatible_sigscheme_for_suites(
    sigscheme: SignatureScheme,
    common_suites: &[&'static SupportedCipherSuite],
) -> bool {
    let sigalg = sigscheme.sign();
    common_suites
        .iter()
        .any(|&suite| suite.usable_for_sigalg(sigalg))
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::msgs::enums::CipherSuite;

    #[test]
    fn test_client_pref() {
        let client = vec![
            CipherSuite::TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
            CipherSuite::TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
        ];
        let server = vec![
            &TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
            &TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
        ];
        let chosen = choose_ciphersuite_preferring_client(&client, &server);
        assert!(chosen.is_some());
        assert_eq!(chosen.unwrap(), &TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256);
    }

    #[test]
    fn test_server_pref() {
        let client = vec![
            CipherSuite::TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
            CipherSuite::TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
        ];
        let server = vec![
            &TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
            &TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
        ];
        let chosen = choose_ciphersuite_preferring_server(&client, &server);
        assert!(chosen.is_some());
        assert_eq!(chosen.unwrap(), &TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384);
    }

    #[test]
    fn test_pref_fails() {
        assert!(
            choose_ciphersuite_preferring_client(
                &[CipherSuite::TLS_NULL_WITH_NULL_NULL],
                &ALL_CIPHERSUITES
            )
            .is_none()
        );
        assert!(
            choose_ciphersuite_preferring_server(
                &[CipherSuite::TLS_NULL_WITH_NULL_NULL],
                &ALL_CIPHERSUITES
            )
            .is_none()
        );
    }

    #[test]
    fn test_scs_is_debug() {
        println!("{:?}", ALL_CIPHERSUITES);
    }

    #[test]
    fn test_usable_for_version() {
        fn ok_tls13(scs: &SupportedCipherSuite) {
            assert!(!scs.usable_for_version(ProtocolVersion::TLSv1_0));
            assert!(!scs.usable_for_version(ProtocolVersion::TLSv1_2));
            assert!(scs.usable_for_version(ProtocolVersion::TLSv1_3));
        }

        fn ok_tls12(scs: &SupportedCipherSuite) {
            assert!(!scs.usable_for_version(ProtocolVersion::TLSv1_0));
            assert!(scs.usable_for_version(ProtocolVersion::TLSv1_2));
            assert!(!scs.usable_for_version(ProtocolVersion::TLSv1_3));
        }

        ok_tls13(&TLS13_CHACHA20_POLY1305_SHA256);
        ok_tls13(&TLS13_AES_256_GCM_SHA384);
        ok_tls13(&TLS13_AES_128_GCM_SHA256);

        ok_tls12(&TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256);
        ok_tls12(&TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256);
        ok_tls12(&TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384);
        ok_tls12(&TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256);
        ok_tls12(&TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384);
    }

    #[test]
    fn test_can_resume_to() {
        assert!(TLS13_CHACHA20_POLY1305_SHA256.can_resume_to(&TLS13_AES_128_GCM_SHA256));
        assert!(!TLS13_CHACHA20_POLY1305_SHA256.can_resume_to(&TLS13_AES_256_GCM_SHA384));
        assert!(
            !TLS13_CHACHA20_POLY1305_SHA256
                .can_resume_to(&TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256)
        );
        assert!(
            !TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
                .can_resume_to(&TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256)
        );
        assert!(
            TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256
                .can_resume_to(&TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256)
        );
    }
}
