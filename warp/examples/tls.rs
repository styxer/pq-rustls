#![deny(warnings)]

// Don't copy this `cfg`, it's only needed because this file is within
// the warp repository.
// Instead, specify the "tls" feature in your warp dependency declaration.
#[cfg(feature = "tls")]
#[tokio::main]
async fn main() {
    use warp::Filter;

    // Match any request and return hello world!
    let routes = warp::any().map(|| "Hello, World!");

    warp::serve(routes)
        .tls()
        .cert_path("/mnt/d/Documents/GIT/2021_master_biber/code/openssl/certs/dilithium2/full.chain")
        .key_path("/mnt/d/Documents/GIT/2021_master_biber/code/openssl/certs/dilithium2/dilithium2_srv.key")
        .run(([127, 0, 0, 1], 3030))
        .await;
}

#[cfg(not(feature = "tls"))]
fn main() {
    eprintln!("Requires the `tls` feature.");
}
