#![deny(warnings)]

// This is using the `tokio` runtime. You'll need the following dependency:
//
// `tokio = { version = "1", features = ["full"] }`
#[cfg(not(target_arch = "wasm32"))]
#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let client = reqwest::Client::builder()
                 .use_rustls_tls()
                 .add_root_certificate(reqwest::Certificate::from_pem(include_bytes!("../../openssl/certs/dilithium2/rsa_dilithium2_CA.crt"))?)
                 .build()?;

    let res = client
              .get("https://localhost:3030")
              .send()
              .await?;

    println!("Status: {}", res.status());

    let body = res.text().await?;

    println!("Body: {}", body);

    Ok(())
}

// The [cfg(not(target_arch = "wasm32"))] above prevent building the tokio::main function
// for wasm32 target, because tokio isn't compatible with wasm32.
// If you aren't building for wasm32, you don't need that line.
// The two lines below avoid the "'main' function not found" error when building for wasm32 target.
#[cfg(target_arch = "wasm32")]
fn main() {}
