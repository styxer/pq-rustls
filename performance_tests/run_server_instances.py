import os
import argparse
import subprocess

key_L1_exchange_algorithms = [
    "LIGHT_SABER",
    "KYBER512",
    "KYBER512_90S",
    "CLASSIC_MC_ELIECE_348864",
    "CLASSIC_MC_ELIECE_348864F",
    "NTRU_HPS_2048509",
    "NTRU_PRIME_NTRU_LPR_653",
    "SECP_256_R1"
]

parser = argparse.ArgumentParser()
parser.add_argument("certs_dir", help="Path to the directory which contains all certificate directories", type=str)
parser.add_argument("log_dir", help="The directory where the performance_server should store all logs", type = str)
parser.add_argument("response_body_path", help="The file which should be used for the resoponse body", type=str)
parser.add_argument("--local_test", default=False, action="store_true")
args = parser.parse_args()

if not os.path.isdir(args.certs_dir) or not os.path.isdir(args.log_dir) or not os.path.isfile(args.response_body_path):
    parser.print_help()
    exit()

subdirs = [x[0] for x in os.walk(args.certs_dir)]
subdirs.remove(args.certs_dir)

bin_dir = "./performance_server"
if args.local_test:
    os.environ["RUSTFLAGS"] = "-C target-feature=aes,avx2,sse4,pclmul"
    subprocess.Popen(["cargo",
                      "build",
                      "--release",
                      "--example",
                      "performance_server"]).wait()
    bin_dir = "target/release/examples/performance_server"


print("[+] Using LOG_DIR={}".format(args.log_dir))
print("[+] Using RESPONSE_BODY_PATH={}".format(args.response_body_path))
port = 3030
processes = []
for subdir in sorted(subdirs):
    if "l3" in subdir or \
       "l5" in subdir or \
       "III" in subdir or \
       "V" in subdir or \
       "dilithium3" in subdir or \
       "dilithium5" in subdir:
        continue

    for key_exchange in key_L1_exchange_algorithms:
        test_name = "{}_{}".format(os.path.basename(subdir), key_exchange)
        cert_path = "{}/full.chain".format(subdir)
        key_path = "{}/{}_srv.key".format(subdir, os.path.basename(subdir))
        print("Starting performance server instance:\n\tTEST_NAME: {}\n\tCERT_PATH: {}\n\tKEY_PATH: {}\n\tPORT: {}".format(test_name, cert_path, key_path, port))
        os.environ["RUST_MIN_STACK"] = str(8388608)
        instance = subprocess.Popen([bin_dir,
                                     "-p",
                                     str(port),
                                     "--cert",
                                     cert_path,
                                     "--key",
                                     key_path,
                                     "--testname",
                                     test_name,
                                     "--responsebodypath",
                                     args.response_body_path,
                                     "--logdir",
                                     args.log_dir])

        processes.append(instance)

        port += 1

print("[+] Started {} process".format(port - 3030))
input("Press enter to stop all performance server instances")

for process in processes:
    process.kill()
