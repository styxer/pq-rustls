import argparse
import os
from matplotlib.artist import allow_rasterization
import matplotlib.pyplot as plt
import numpy as np

from read_log_files import read_log_files, calculate_time_delta

parser = argparse.ArgumentParser()
parser.add_argument("logfiles_path", help="Path to the directory which contains all directories with logfiles.", type=str)
args = parser.parse_args()

if not os.path.isdir(args.logfiles_path):
    parser.print_help()
    exit()

client_events, server_events = read_log_files(args.logfiles_path)
client_handshake_times = {}
kx_algos = []

for test_location_setup_name, events in client_events.items():
    client_handshake_times[test_location_setup_name] = calculate_time_delta(events)

    if not kx_algos:
        kx_algos = client_handshake_times[test_location_setup_name]["dilithium2"]["kx_algos"]

location_order = [
    "frankfurt_1_frankfurt_2",
    "paris_frankfurt_1",
    "local_frankfurt_1",
    "ohio_frankfurt_1"
]

event_names = [
    "RequestTime",
    "ClientHandshake",
    "KeyGeneration",
    "KeyDecapsulation",
    "SignatureVerify"
]

location_times = {}
for kx_algo in kx_algos:
    for location in location_order:
        for signature, events in client_handshake_times[location].items():
            if kx_algo not in location_times:
                location_times[kx_algo] = {}
            
            if signature not in location_times[kx_algo]:
                location_times[kx_algo][signature] = {}
                
            for event_name in event_names:
                if event_name not in location_times[kx_algo][signature]:
                    location_times[kx_algo][signature][event_name] = []

                location_times[kx_algo][signature][event_name] += [events[event_name][kx_algos.index(kx_algo)]]

signature_location_times = {}
for location in location_order:
    for signature, events in client_handshake_times[location].items():
        for kx_algo in kx_algos:
            if signature not in signature_location_times:
                signature_location_times[signature] = {}
            
            if kx_algo not in signature_location_times[signature]:
                signature_location_times[signature][kx_algo] = {}
            
            for event_name in event_names:
                if event_name not in signature_location_times[signature][kx_algo]:
                    signature_location_times[signature][kx_algo][event_name] = []

                signature_location_times[signature][kx_algo][event_name] += [events[event_name][kx_algos.index(kx_algo)]]

allowed_kx_algos = [
    "CLASSIC_MC_ELIECE_348864",
    "CLASSIC_MC_ELIECE_348864F",
    "KYBER512_90S",
    "LIGHT_SABER",
    "SECP_256_R1"
]

allowed_signature_verify_kx_algos = [
    "CLASSIC_MC_ELIECE_348864"
]

allowed_signature_algos = [
    "dilithium2",
    "picnic3l1",
    "picnicl1ur",
    "rainbowIcompressed",
    "sphincssha256128frobust",
    "rainier4l1"
]

events_for_plots = [
    ("ClientHandshake", "RequestTime"),
    ("KeyGeneration", "ClientHandshake"),
    ("KeyDecapsulation", "ClientHandshake"),
    ("SignatureVerify", "ClientHandshake")
]


for event_for_plot, reference_event in events_for_plots:
    for kx_algo, times in location_times.items():
        if kx_algo not in allowed_kx_algos:
            continue
        
        print("[+] {}:".format(kx_algo))

        fig, axs = plt.subplots(1, 1, figsize=(20, 20))
        
        for signature_algo, events in times.items():    
            if signature_algo not in allowed_signature_algos:
                continue

            event_index = 0
            last_values = []
            
            reference_time = events[reference_event]
            event_time = events[event_for_plot]
            print("  {} {} {}:".format(signature_algo, event_for_plot, event_time))

            #if event_for_plot == "ClientHandshake":
            #    for index in range(0, len(event_time)):
            #       event_time[index] -= events["SignatureVerify"][index]

            percental_usage = []
            for index in range(0, len(reference_time)):
                percental_usage.append((event_time[index] / reference_time[index]) * 100)
            
            axs.plot(np.arange(len(percental_usage)), percental_usage, "x-", label=signature_algo)

            axs.set_title("{} {} benchmark".format(kx_algo, event_for_plot), fontsize=40) 
            axs.set_ylabel("% of {}".format(reference_event), fontsize=35)
            axs.legend(prop={'size': 30})
            axs.set_xticks(np.arange(len(percental_usage)))
            axs.set_xticklabels(["Frankfurt", "Paris", "Graz", "Ohio"], rotation=90, fontsize=30)
            axs.grid(True)

        plt.yticks(fontsize=30)
        fig.tight_layout()
        plt.savefig("graphs/times_per_testsetup/{}_{}_client.png".format(kx_algo, event_for_plot))

fig, axs = plt.subplots(1, 1, figsize=(20, 20))
for signature_algo, times in signature_location_times.items():
    if signature_algo not in allowed_signature_algos:
        continue
    
    print("[+] {}:".format(signature_algo))
                                                                                               
    percental_usage = []
    for kx_algo, events in times.items():    
        if kx_algo not in allowed_signature_verify_kx_algos:
            continue
                                                                                               
        event_index = 0
        last_values = []
        
        reference_time = events["ClientHandshake"]
        event_time = events["SignatureVerify"]
                                                                                               
        for index in range(0, len(reference_time)):
            if len(percental_usage) < len(reference_time):
                percental_usage.append((event_time[index] / reference_time[index]) * 100)
            else:
                percental_usage[index] += (event_time[index] / reference_time[index]) * 100
        
    for index in range(0, len(percental_usage)):
        percental_usage[index] /= len(allowed_signature_verify_kx_algos)

    print("  {} {} {}:".format(signature_algo, "SignatureVerify", percental_usage))

    axs.plot(np.arange(len(percental_usage)), percental_usage, "x-", label=signature_algo)
                                                                                           
axs.set_title("{} benchmark".format("SignatureVerify"), fontsize=40) 
axs.set_ylabel("% of {}".format("ClientHandshake"), fontsize=35)
axs.legend(prop={'size': 30})
axs.set_xticks(np.arange(4))
axs.set_xticklabels(["Frankfurt", "Paris", "Graz", "Ohio"], rotation=90, fontsize=30)
axs.grid(True)
                                                                                               
plt.yticks(fontsize=30)
fig.tight_layout()
plt.savefig("graphs/times_per_testsetup/{}_{}_client.png".format("SignatureVerify", "ClientHandshake"))
