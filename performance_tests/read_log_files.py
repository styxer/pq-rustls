import os
import glob
import json
import re
import datetime


def read_log_files(logfiles_path: str):
    server_events = {}
    client_events = {}

    for top_level_directory in os.walk(logfiles_path):   
        if "client" in top_level_directory[0] or "server" in top_level_directory[0] or top_level_directory[0] == logfiles_path:
            continue
            
        for directory in [top_level_directory[0] + "/client", top_level_directory[0] + "/server"]:
            log_list = glob.glob('{}/*.log'.format(directory))
            test_location_setup_name = os.path.basename(top_level_directory[0])
            
            for logfile in log_list:
                if "PRIME" in logfile:
                    continue

                with open(logfile, "r") as file:
                    lines = file.readlines()
                    for line in lines:
                        parsed_json = json.loads(line)
                        test_name = parsed_json["fields"]["test_name"]
                        entry = {
                            "timestamp": parsed_json["timestamp"],
                            "event_name": parsed_json["fields"]["event_name"],
                            "span": parsed_json["spans"][0]["name"]
                        }
                        signature_name = re.findall("([a-zA-Z0-9]+)_", test_name)[0]
                        key_exchange_name = test_name.replace(signature_name + "_", "")

                        if "ServerEvents" in logfile:
                            if test_location_setup_name not in server_events:
                                server_events[test_location_setup_name] = {}

                            if signature_name not in server_events[test_location_setup_name]:
                                server_events[test_location_setup_name][signature_name] = {}
                                server_events[test_location_setup_name][signature_name][key_exchange_name] = [entry]
                            else:
                                if key_exchange_name not in server_events[test_location_setup_name][signature_name]:
                                    server_events[test_location_setup_name][signature_name][key_exchange_name] = [entry]
                                else:
                                    server_events[test_location_setup_name][signature_name][key_exchange_name].append(entry)
                        elif "ClientEvents" in logfile:
                            if test_location_setup_name not in client_events:
                                client_events[test_location_setup_name] = {}

                            if signature_name not in client_events[test_location_setup_name]:
                                client_events[test_location_setup_name][signature_name] = {}
                                client_events[test_location_setup_name][signature_name][key_exchange_name] = [entry]
                            else:
                                if key_exchange_name not in client_events[test_location_setup_name][signature_name]:
                                    client_events[test_location_setup_name][signature_name][key_exchange_name] = [entry]
                                else:
                                    client_events[test_location_setup_name][signature_name][key_exchange_name].append(entry)

    return client_events, server_events


def calculate_time_delta(events):
    legend_mapping = {
        "ServerHandshakeStart": "ServerHandshake",
        "ClientHandshakeStart": "ClientHandshake",
        "KeyGenerationStart": "KeyGeneration",
        "KeyDecapsulationStart": "KeyDecapsulation",
        "KeyEncapsulationStart": "KeyEncapsulation",
        "SignatureVerifyStart": "SignatureVerify",
        "RequestStart": "RequestTime",
        "SignatureSignStart": "SignatureSign"
    }

    event_time_mapping = {}
    max_delta = 0
    for signature, key_exchange_algorithms in events.items():
        for key, events in key_exchange_algorithms.items():
            for start_event in events:
                if "End" in start_event["event_name"]:
                    continue

                event_name = start_event["event_name"]
                end_event_name = event_name.replace("Start", "End")

                corresponding_end_event = None
                for end_event in events:
                    if end_event["event_name"] == end_event_name:
                        corresponding_end_event = end_event
                        break
               
                if corresponding_end_event == None:
                    continue
                
                start_time = datetime.datetime.strptime(start_event["timestamp"], '%Y-%m-%dT%H:%M:%S.%fZ')
                end_time   = datetime.datetime.strptime(corresponding_end_event["timestamp"], '%Y-%m-%dT%H:%M:%S.%fZ')
                delta      = end_time - start_time
                
                delta_milliseconds = round(delta.total_seconds() * 1e3, 3)
                max_delta = max(delta_milliseconds, max_delta)
                
                if signature not in event_time_mapping:
                    event_time_mapping[signature] = {}
                    event_time_mapping[signature][legend_mapping[event_name]] = [delta_milliseconds]
                    event_time_mapping[signature]["kx_algos"] = [key]
                else:
                    if legend_mapping[event_name] not in event_time_mapping[signature]:
                        event_time_mapping[signature][legend_mapping[event_name]] = [delta_milliseconds]
                        event_time_mapping[signature]["kx_algos"] = [key]
                    else:
                        event_time_mapping[signature][legend_mapping[event_name]].append(delta_milliseconds)
                        if key not in event_time_mapping[signature]["kx_algos"]:
                            event_time_mapping[signature]["kx_algos"].append(key)
                
    return event_time_mapping
