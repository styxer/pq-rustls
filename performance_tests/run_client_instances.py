import os
import argparse
import subprocess

key_L1_exchange_algorithms = [
    "LIGHT_SABER",
    "KYBER512",
    "KYBER512_90S",
    "CLASSIC_MC_ELIECE_348864",
    "CLASSIC_MC_ELIECE_348864F",
    "NTRU_HPS_2048509",
    "NTRU_PRIME_NTRU_LPR_653",
    "SECP_256_R1"
]

parser = argparse.ArgumentParser()
parser.add_argument("server_address", help="DNS name or IP of the performance_server", type=str)
parser.add_argument("certs_dir", help="Path to the directory which contains all certificate directories", type=str)
parser.add_argument("response_body_output", help="The file where the server response should be written to", type=str)
parser.add_argument("log_dir", help="The directory where the performance_server should store all logs", type = str)
parser.add_argument("--local-test", default=False, action="store_true")
args = parser.parse_args()

if not os.path.isdir(args.certs_dir) or \
   not os.path.isdir(args.log_dir):
    parser.print_help()
    exit()

subdirs = [x[0] for x in os.walk(args.certs_dir)]
subdirs.remove(args.certs_dir)

bin_dir = "./performance_client"
if args.local_test:
    os.environ["RUSTFLAGS"] = "-C target-feature=aes,avx2,sse4,pclmul"
    subprocess.Popen(["cargo",
                      "build",
                      "--release",
                      "--example",
                      "performance_client"]).wait()
    bin_dir = "target/release/examples/performance_client"


print("[+] Using LOG_DIR={}".format(args.log_dir))
print("[+] Using RESPONSE_BODY_OUTPUT={}".format(args.response_body_output))

port = 3030
for subdir in sorted(subdirs):
    if "l3" in subdir or \
       "l5" in subdir or \
       "III" in subdir or \
       "V" in subdir or \
       "dilithium3" in subdir or \
       "dilithium5" in subdir:
        continue

    for key_exchange in key_L1_exchange_algorithms:
        kx_group = key_exchange
        test_name = "{}_{}".format(os.path.basename(subdir), key_exchange)
        cert_path = "{}/{}_{}_CA.crt".format(subdir,
                                             os.path.basename(subdir),
                                             os.path.basename(subdir))
        print("Starting performance client instance:\n\tTEST_NAME: {}\n\tCERT_PATH: {}\n\tPORT: {}\n\tKX_GROUP: {}".format(test_name, cert_path, port, kx_group))
        os.environ["RUST_MIN_STACK"] = str(8388608)
        subprocess.Popen([bin_dir,
                          args.server_address,
                          "-p",
                          str(port),
                          "--cert",
                          cert_path,
                          "--testname",
                          test_name,
                          "--bodyoutput",
                          args.response_body_output,
                          "--logdir",
                          args.log_dir,
                          "--kxgroup",
                          kx_group]).wait()

        port += 1
