import argparse
import os

from read_log_files import read_log_files, calculate_time_delta

parser = argparse.ArgumentParser()
parser.add_argument("logfiles_path", help="Path to the directory which contains all directories with logfiles.", type=str)
args = parser.parse_args()

if not os.path.isdir(args.logfiles_path):
    parser.print_help()
    exit()

client_events, _ = read_log_files(args.logfiles_path)
locations = list(client_events.keys())

signature_algos = list(client_events[locations[0]].keys())
signature_algos.remove("falcon1024")
kx_algos = list(client_events[locations[0]][signature_algos[0]].keys())

caption_mapping = {
    "frankfurt_1_frankfurt_2": ("Frankfurt 1", "Frankfurt 2"),
    "local_frankfurt_1": ("Graz", "Frankfurt"),
    "paris_frankfurt_1": ("Paris", "Frankfurt"),
    "ohio_frankfurt_1": ("Ohio", "Frankfurt")
}

kx_algo_mapping = {
    "CLASSIC_MC_ELIECE_348864": "ClassicMcEliece 348864",
    "CLASSIC_MC_ELIECE_348864F": "ClassicMcEliece 348864F",
    "KYBER512": "Kyber512",
    "KYBER512_90S": "Kyber512 90s",
    "LIGHT_SABER": "Light Saber",
    "NTRU_HPS_2048509": "NTRU HPS 2048509",
    "SECP_256_R1": "SECP 256 R1"
}

signature_algo_mapping = {
    "dilithium2": "Dilithium 2",
    "ecdsa": "Ecdsa",
    "falcon1024": "Falcon 1024",
    "falcon512": "Falcon 512",
    "picnic3l1": "Picnic3 L1",
    "picnicl1fs": "Picnic L1 FS",
    "picnicl1ur": "Picnic L1 UR",
    "picnicl1full": "Picnic L1 FULL",
    "rainbowIcircumzenithal": "Rainbow I Circumzenithal",
    "rainbowIclassic": "Rainbow I Classic",
    "rainbowIcompressed": "Rainbow I Compressed",
    "rainier3l1": "Rainier3 L1",
    "rainier4l1": "Rainier4 L1",
    "sphincssha256128frobust": "Sphincs SHA-256F Robust",
    "sphincssha256128fsimple": "Sphincs SHA-256F Simple",
    "sphincssha256128srobust": "Sphincs SHA-256S Robust",
    "sphincssha256128ssimple": "Sphincs SHA-256S Simple"
}

event_name = "ClientHandshake" #"RequestTime"
for location in locations:
    with open("tables/{}_matrix_table.tex".format(location), "w+") as table:
        table.write("""\\footnotesize\n""" \
                    """\\begin{longtable}{ l r r r r r r r }\n""" \
                    """\\caption{\\texttt{""" + caption_mapping[location][0] + """} $\\rightarrow$ \\texttt{""" + caption_mapping[location][1] + """} \\texttt{""" + event_name + """} in \\texttt{ms}.}\\\\\n""" \
                    """\\toprule\n""")

        for kx_algo in kx_algos:
            table.write("& \\begin{turn}{90} \\texttt{" + kx_algo_mapping[kx_algo] + "} \\end{turn} ")

        table.write("\\\\\n\\midrule\n")

        for signature_algo in signature_algos:
            table.write("\\texttt{" + signature_algo_mapping[signature_algo] + "} ")

            index = 0
            for time in calculate_time_delta(client_events[location])[signature_algo][event_name]:
                if (signature_algo == "dilithium2" or signature_algo == "ecdsa" or "sphincs" in signature_algo) and (index < 4 or index == 6):
                    table.write("& \\textbf{{{:.2f}}} ".format(round(time, 2)))
                else:
                    table.write("& {:.2f}".format(round(time, 2)))

                index += 1

            table.write("\\\\\n")

        table_end = """\\toprule\n""" \
                                        """\\label{tab:""" + location + """_matrix_table}\n""" \
                    """\\end{longtable}\n""" \
                    """\\normalsize"""

        table.write(table_end)

    for signature_algo in signature_algos:
        if not "dilithium" in signature_algo and not "sphincs" in signature_algo:
            continue

        with open("tables/{}_{}_detailed_times_table.tex".format(location, signature_algo), "w+") as table:

                table.write("""\\footnotesize\n""" \
                            """\\begin{longtable}{l r r r r r}\n""" \
                            """\\caption[Detailed \\texttt{""" +  caption_mapping[location][0] + """} $\\rightarrow$ \\texttt{""" + caption_mapping[location][1]  + """}]{Detailed \\texttt{""" +  caption_mapping[location][0] + """} $\\rightarrow$ \\texttt{""" + caption_mapping[location][1]  + """} \\texttt{""" + signature_algo_mapping[signature_algo] + """} benchmarks.} \\\\\n""" \
                            """\\toprule\n""")

                table.write("""\\multicolumn{6}{c}{""" + signature_algo_mapping[signature_algo] + """}\\\\\n""")
                table.write("\\midrule\\\\\n")
                table.write("\\textbf{Algorithm} & \\textbf{ReqTime} & \\textbf{ClientHandshake} & \\textbf{KeyGen.} & \\textbf{KeyDec.} & \\textbf{SigVerify} \\\\\n")
                table.write("& (ms) & (ms) & (ms) & (ms) & (ms) \\\\\n")
                table.write("\\midrule\\\\\n")
                time_delta = calculate_time_delta(client_events[location])
                kx_algos = time_delta[signature_algo]["kx_algos"]
                for kx_algo in ["CLASSIC_MC_ELIECE_348864", "CLASSIC_MC_ELIECE_348864F", "KYBER512", "KYBER512_90S", "SECP_256_R1"]:
                    table.write(kx_algo_mapping[kx_algo] + " ")
                    for key, value in time_delta[signature_algo].items():
                        if key == "kx_algos":
                            continue

                        table.writelines("& {:.2f} ".format(round(value[kx_algos.index(kx_algo)], 2)))

                    table.write("\\\\\n")

                table.write("""\\toprule\n""" \
                            """\\label{tab:""" + location + """_""" + signature_algo + """_detailed_times_table}\n""" \
                            """\\end{longtable}\n""" \
                            """\\normalsize\n""")

