#!/usr/bin/env python3

import json
import argparse
import os
import glob
import re

from read_log_files import calculate_time_delta

parser = argparse.ArgumentParser()
parser.add_argument("logfiles_path", help="Path to the directory which contains all logfiles.", type=str)
args = parser.parse_args()

if not os.path.isdir(args.logfiles_path):
    parser.print_help()
    exit()

kx_algos_benchmark = {}
signature_algos_benchmark = {}

for top_level_directory in os.walk(args.logfiles_path):
    if "client" in top_level_directory[0] or "server" in top_level_directory[0] or top_level_directory[0] == args.logfiles_path:
        continue

    for directory in [top_level_directory[0] + "/client", top_level_directory[0] + "/server"]:
        log_list = glob.glob('{}/*.log'.format(directory))
        server_events = {}
        client_events = {}

        for logfile in log_list:
            if "PRIME" in logfile or "falcon1024" in logfile:
                continue

            with open(logfile, "r") as file:
                lines = file.readlines()
                for line in lines:
                    parsed_json = json.loads(line)
                    test_name = parsed_json["fields"]["test_name"]
                    entry = {
                        "timestamp": parsed_json["timestamp"],
                        "event_name": parsed_json["fields"]["event_name"],
                        "span": parsed_json["spans"][0]["name"]
                    }
                    signature_name = re.findall("([a-zA-Z0-9]+)_", test_name)[0]
                    key_exchange_name = test_name.replace(signature_name + "_", "")

                    if "ServerEvents" in logfile:
                        if signature_name not in server_events:
                            server_events[signature_name] = {}
                            server_events[signature_name][key_exchange_name] = [entry]
                        else:
                            if key_exchange_name not in server_events[signature_name]:
                                server_events[signature_name][key_exchange_name] = [entry]
                            else:
                                server_events[signature_name][key_exchange_name].append(entry)
                    elif "ClientEvents" in logfile:
                        if signature_name not in client_events:
                            client_events[signature_name] = {}
                            client_events[signature_name][key_exchange_name] = [entry]
                        else:
                            if key_exchange_name not in client_events[signature_name]:
                                client_events[signature_name][key_exchange_name] = [entry]
                            else:
                                client_events[signature_name][key_exchange_name].append(entry)

        client_event_time_mapping = calculate_time_delta(client_events)
        server_event_time_mapping = calculate_time_delta(server_events)

        kx_times = {}
        if client_event_time_mapping:
            for signature_algo, measurements in client_event_time_mapping.items():
                if signature_algo not in signature_algos_benchmark:
                    signature_algos_benchmark[signature_algo] = {
                        "SignatureVerify": [],
                        "SignatureSign": []
                    }

                signature_algos_benchmark[signature_algo]["SignatureVerify"] += [round(sum(measurements["SignatureVerify"]) / len(measurements["SignatureVerify"]), 3)]

                for kx_algo in measurements["kx_algos"]:
                    if kx_algo not in kx_times:
                        kx_times[kx_algo] = {
                            "KeyGeneration": [],
                            "KeyDecapsulation": [],
                            "KeyEncapsulation": []
                        }

                    kx_times[kx_algo]["KeyGeneration"] += [measurements["KeyGeneration"][measurements["kx_algos"].index(kx_algo)]]
                    kx_times[kx_algo]["KeyDecapsulation"] += [measurements["KeyDecapsulation"][measurements["kx_algos"].index(kx_algo)]]

            for kx_algo, times in kx_times.items():
                if kx_algo not in kx_algos_benchmark:
                    kx_algos_benchmark[kx_algo] = {
                        "KeyGeneration": [],
                        "KeyDecapsulation": [],
                        "KeyEncapsulation": []
                    }

                kx_algos_benchmark[kx_algo]["KeyGeneration"] += [round(sum(kx_times[kx_algo]["KeyGeneration"]) / len(kx_times[kx_algo]["KeyGeneration"]), 3)]
                kx_algos_benchmark[kx_algo]["KeyDecapsulation"] += [round(sum(kx_times[kx_algo]["KeyDecapsulation"]) / len(kx_times[kx_algo]["KeyDecapsulation"]), 3)]
        elif server_event_time_mapping:
            for signature_algo, measurements in server_event_time_mapping.items():
                if signature_algo not in signature_algos_benchmark:
                    signature_algos_benchmark[signature_algo] = {
                        "SignatureVerify": [],
                        "SignatureSign": []
                    }

                signature_algos_benchmark[signature_algo]["SignatureSign"] += [round(sum(measurements["SignatureSign"]) / len(measurements["SignatureSign"]), 3)]

                for kx_algo in measurements["kx_algos"]:
                    if kx_algo not in kx_times:
                        kx_times[kx_algo] = {
                            "KeyGeneration": [],
                            "KeyDecapsulation": [],
                            "KeyEncapsulation": []
                        }

                    kx_times[kx_algo]["KeyEncapsulation"] += [measurements["KeyEncapsulation"][measurements["kx_algos"].index(kx_algo)]]

            for kx_algo, times in kx_times.items():
                if kx_algo not in kx_algos_benchmark:
                    kx_algos_benchmark[kx_algo] = {
                        "KeyGeneration": [],
                        "KeyDecapsulation": [],
                        "KeyEncapsulation": []
                    }

                kx_algos_benchmark[kx_algo]["KeyEncapsulation"] += [round(sum(kx_times[kx_algo]["KeyEncapsulation"]) / len(kx_times[kx_algo]["KeyEncapsulation"]), 3)]

print("[+] SignatureSign:")
for signature_algo, measurements in signature_algos_benchmark.items():
    print("\t{}: {:.3f} ms".format(signature_algo, round(sum(measurements["SignatureSign"]) / len(measurements["SignatureSign"]), 3)))

print("[+] SignatureVerify:")
for signature_algo, measurements in signature_algos_benchmark.items():
    print("\t{}: {:.3f} ms".format(signature_algo, round(sum(measurements["SignatureVerify"]) / len(measurements["SignatureVerify"]), 3)))

print("[+] KeyGeneration:")
for kx_algo, measurements in kx_algos_benchmark.items():
    print("\t{}: {:.3f} ms".format(kx_algo, round(sum(measurements["KeyGeneration"]) / len(measurements["KeyGeneration"]), 3)))

print("[+] KeyEncapsulation:")
for kx_algo, measurements in kx_algos_benchmark.items():
    print("\t{}: {:.3f} ms".format(kx_algo, round(sum(measurements["KeyEncapsulation"]) / len(measurements["KeyEncapsulation"]), 3)))

print("[+] KeyDecapsulation:")
for kx_algo, measurements in kx_algos_benchmark.items():
    print("\t{}: {:.3f} ms".format(kx_algo, round(sum(measurements["KeyDecapsulation"]) / len(measurements["KeyDecapsulation"]), 3)))

