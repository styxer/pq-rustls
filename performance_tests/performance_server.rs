use warp::{
    http::StatusCode,
    Filter, Rejection, Reply,
};

use std::convert::Infallible;

#[macro_use]
extern crate serde_derive;

use docopt::Docopt;

use tracing_subscriber;
use tracing;

use std::sync::Mutex;
use std::fs::File;

//-----------------------------------------------------------------------------

const USAGE: &'static str = "
Usage:
  performance_server [options] -p <PORT> --cert <CERT_FILE> --key <KEYFILE> --testname <TEST_NAME> --responsebodypath <RESPONSE_BODY_PATH> --logdir <LOG_DIR>
  performance_server (--help | -h)

Options:
    -p, --port PORT                      Connect to PORT.
    --cert CERTFILE                      Read server certificates from CERTFILE. 
                                         This should contain PEM-format certificates in the right order (the first certificate should
                                         certify KEYFILE, the last should be a root CA).
    --key KEYFILE                        Read private key from KEYFILE.  This should be a RSA private key or PKCS8-encoded private key, in PEM format.
    --testname NAME                      Name of the current running performance test.
    --responsebodypath RESPONSEBODYPATH  Response which should be send to the client.
    --logdir LOGDIR                      Directory where the logfiles should be saved.
    -v, --verbose                        Enable logging output.
    --help, -h                           Show this screen.
";

//-----------------------------------------------------------------------------

#[derive(Debug, Deserialize)]
struct Args {
    flag_port: u16,
    flag_cert: String,
    flag_key: String,
    flag_testname: String,
    flag_responsebodypath: String,
    flag_logdir: String,
    flag_verbose: bool,
}

//-----------------------------------------------------------------------------

#[tokio::main]
async fn main() {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| Ok(d.help(true)))
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    let borrowed_args = &args;
    if borrowed_args.flag_verbose {
        env_logger::Builder::new()
            .parse_filters("trace")
            .init();
    }
    else {
        let log_file = File::create(format!("{}/ServerEvents_{}.log", borrowed_args.flag_logdir.clone(), borrowed_args.flag_testname.clone())).expect("Could not creat log file");

        tracing_subscriber::fmt()
                            .with_writer(Mutex::new(log_file))
                            .json()
                            .with_max_level(tracing::Level::TRACE)
                            .with_current_span(false)
                            .with_env_filter("PERF_TEST")
                            .init();
    }
    let routes = warp::get()
                 .and(warp::path(borrowed_args.flag_testname.clone()))
                 .and(warp::fs::file(borrowed_args.flag_responsebodypath.clone()))
                 .recover(handle_rejection);

    warp::serve(routes)
        .tls()
        .cert_path(&borrowed_args.flag_cert)
        .key_path(&borrowed_args.flag_key)
        .test_name(borrowed_args.flag_testname.clone())
        .run(([0, 0, 0, 0], borrowed_args.flag_port))
        .await;
}

//-----------------------------------------------------------------------------

async fn handle_rejection(err: Rejection) -> std::result::Result<impl Reply, Infallible> {
    let (code, message) = if err.is_not_found() {
        (StatusCode::NOT_FOUND, "File not found".to_string())
    } else if err.find::<warp::reject::PayloadTooLarge>().is_some() {
        (StatusCode::BAD_REQUEST, "Payload too large".to_string())
    } else {
        eprintln!("unhandled error: {:?}", err);
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Internal Server Error".to_string(),
        )
    };

    Ok(warp::reply::with_status(message, code))
}