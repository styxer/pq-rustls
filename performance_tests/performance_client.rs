use std::io::prelude::*;
use std::fs::File;

#[macro_use]
extern crate serde_derive;

use docopt::Docopt;

use env_logger;

use tracing_subscriber;
use tracing::{event, Level, span};

use std::sync::Mutex;

//-----------------------------------------------------------------------------

const USAGE: &'static str = "
Usage:
  performance_client [options] <host> -p <PORT> --cert <CERT_FILE> --kxgroup <KX_GROUP> --testname <TEST_NAME> --bodyoutput <BODY_OUTPUT_PATH> --logdir <LOG_DIR>
  performance_client (--help | -h)

Options:
    -p, --port PORT              Connect to PORT.
    --cert CERTFILE              Read client certificates from CERTFILE.
    --kxgroup KXGROUP            The KX group which should be used.           
    --testname NAME              Name of the current running performance test.
    --bodyoutput BODYOUTPUTPATH  Expected body from server.
    --logdir LOGDIR              Directory where the logfiles should be saved.
    -v, --verbose                Enable logging output.
    --help, -h                   Show this screen.
";

//-----------------------------------------------------------------------------

#[derive(Debug, Deserialize)]
struct Args {
    arg_host: String,
    flag_port: u16,
    flag_cert: String,
    flag_kxgroup: String,
    flag_testname: String,
    flag_bodyoutput: String,
    flag_logdir: String,
    flag_verbose: bool
}

//-----------------------------------------------------------------------------

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| Ok(d.help(true)))
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    let borrowed_args = &args;

    if borrowed_args.flag_verbose {
        env_logger::Builder::new()
                .parse_filters("trace")
                .init();
    }

    let kx_group = match borrowed_args.flag_kxgroup.as_str() {
        "SABER"                      => reqwest::NamedGroup::SABER,
        "LIGHT_SABER"                => reqwest::NamedGroup::LIGHT_SABER,
        "FIRE_SABER"                 => reqwest::NamedGroup::FIRE_SABER,
        "KYBER512"                   => reqwest::NamedGroup::KYBER512,
        "KYBER768"                   => reqwest::NamedGroup::KYBER768,
        "KYBER1024"                  => reqwest::NamedGroup::KYBER1024,
        "KYBER512_90S"               => reqwest::NamedGroup::KYBER512_90S,
        "KYBER768_90S"               => reqwest::NamedGroup::KYBER768_90S,
        "KYBER1024_90S"              => reqwest::NamedGroup::KYBER1024_90S,
        "CLASSIC_MC_ELIECE_348864"   => reqwest::NamedGroup::CLASSIC_MC_ELIECE_348864,
        "CLASSIC_MC_ELIECE_348864F"  => reqwest::NamedGroup::CLASSIC_MC_ELIECE_348864F,
        "CLASSIC_MC_ELIECE_460896"   => reqwest::NamedGroup::CLASSIC_MC_ELIECE_460896,
        "CLASSIC_MC_ELIECE_460896F"  => reqwest::NamedGroup::CLASSIC_MC_ELIECE_460896F,
        "CLASSIC_MC_ELIECE_6688128"  => reqwest::NamedGroup::CLASSIC_MC_ELIECE_6688128,
        "CLASSIC_MC_ELIECE_6688128F" => reqwest::NamedGroup::CLASSIC_MC_ELIECE_6688128F,
        "CLASSIC_MC_ELIECE_6960119"  => reqwest::NamedGroup::CLASSIC_MC_ELIECE_6960119, 
        "CLASSIC_MC_ELIECE_6960119F" => reqwest::NamedGroup::CLASSIC_MC_ELIECE_6960119F,
        "CLASSIC_MC_ELIECE_8192128"  => reqwest::NamedGroup::CLASSIC_MC_ELIECE_8192128,
        "CLASSIC_MC_ELIECE_8192128F" => reqwest::NamedGroup::CLASSIC_MC_ELIECE_8192128F,
        "NTRU_HPS_2048509"           => reqwest::NamedGroup::NTRU_HPS_2048509,
        "NTRU_HPS_2048677"           => reqwest::NamedGroup::NTRU_HPS_2048677,
        "NTRU_HPS_4096821"           => reqwest::NamedGroup::NTRU_HPS_4096821,
        "NTRU_HRSS_701"              => reqwest::NamedGroup::NTRU_HRSS_701,
        "NTRU_PRIME_NTRU_LPR_653"    => reqwest::NamedGroup::NTRU_PRIME_NTRU_LPR_653,
        "NTRU_PRIME_NTRU_LPR_761"    => reqwest::NamedGroup::NTRU_PRIME_NTRU_LPR_761,
        "NTRU_PRIME_NTRU_LPR_857"    => reqwest::NamedGroup::NTRU_PRIME_NTRU_LPR_857,
        "NTRU_PRIME_S_NTRU_P_653"    => reqwest::NamedGroup::NTRU_PRIME_S_NTRU_P_653,
        "NTRU_PRIME_S_NTRU_P_761"    => reqwest::NamedGroup::NTRU_PRIME_S_NTRU_P_761,
        "NTRU_PRIME_S_NTRU_P_857"    => reqwest::NamedGroup::NTRU_PRIME_S_NTRU_P_857,
        "SECP_256_R1"                => reqwest::NamedGroup::secp256r1,
        _ => panic!("KX GROUP NOT RECOGNIZED!")
    };

    let mut f = File::open(borrowed_args.flag_cert.clone()).expect("Could not open CERT file");
    let mut buffer = Vec::new();
    f.read_to_end(&mut buffer).expect("Could not read CERT file");
    
    if !args.flag_verbose {
        let log_file = File::create(format!("{}/ClientEvents_{}.log",borrowed_args.flag_logdir, borrowed_args.flag_testname.clone())).expect("Could not creat log file");

        tracing_subscriber::fmt()
                            .with_writer(Mutex::new(log_file))
                            .json()
                            .with_max_level(tracing::Level::TRACE)
                            .with_current_span(false)
                            .with_env_filter("PERF_TEST")
                            .init();
    }

    let client = reqwest::Client::builder()
                 .use_rustls_tls()
                 .add_root_certificate(reqwest::Certificate::from_pem(&buffer)?)
                 .set_default_kx_group(kx_group)
                 .set_performance_test_name(borrowed_args.flag_testname.clone())
                 .build()?;

    let span = span!(target: "PERF_TEST", Level::TRACE, "Request");
    let _guard = span.enter();

    event!(target: "PERF_TEST", Level::TRACE, event_name = "RequestStart", test_name = borrowed_args.flag_testname.clone().as_str());

    let res = client
              .get(format!("https://{}:{}/{}/", borrowed_args.arg_host.clone(), borrowed_args.flag_port, borrowed_args.flag_testname.clone()))
              .send()
              .await?;
    
    let status = res.status();
    let body = res.text().await?;
    
    event!(target: "PERF_TEST", Level::TRACE, event_name = "RequestEnd", test_name = borrowed_args.flag_testname.clone().as_str());

    let mut output = File::create(args.flag_bodyoutput).expect("Could not create body output file");
    write!(output, "{}\n{}", status, body.to_owned()).expect("Could not write to file");

    Ok(())
}
