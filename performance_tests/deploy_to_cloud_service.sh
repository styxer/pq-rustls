#!/bin/bash

cd ../openssl
bash certs/build_certs.sh

cd -

ssh -i $3 -f 'rm -rf /home/$1/certs'
scp -i $3 -r ../openssl/certs/ $1@$2:/home/$1/certs
scp -i $3 run_server_instances.py $1@$2:/home/$1/
scp -i $3 run_client_instances.py $1@$2:/home/$1/
scp -i $3 aws_run.sh $1@$2:/home/$1/

RUSTFLAGS="-C target-feature=aes,avx2,sse4,pclmul" cargo build --release --example performance_server
RUSTFLAGS="-C target-feature=aes,avx2,sse4,pclmul" cargo build --release --example performance_client

scp -i $3 ../../target/release/examples/performance_server $1@$2:/home/$1/
scp -i $3 ../../target/release/examples/performance_client $1@$2:/home/$1/

