#!/usr/bin/env python3

import json
import argparse
import os
import glob
import datetime
import matplotlib.pyplot as plt
import numpy as np
import re

def calculate_time_delta(events, image_postfix):
    legend_mapping = {
        "ServerHandshakeStart": "ServerHandshake",
        "ClientHandshakeStart": "ClientHandshake",
        "KeyGenerationStart": "KeyGeneration",
        "KeyDecapsulationStart": "KeyDecapsulation",
        "KeyEncapsulationStart": "KeyEncapsulation",
        "SignatureVerifyStart": "SignatureVerify",
        "RequestStart": "RequestTime",
        "SignatureSignStart": "SignatureSign"
    }

    event_time_mapping = {}
    max_delta = 0
    for signature, key_exchange_algorithms in events.items():
        fig, axs = plt.subplots(1, 1, figsize=(15, 20))
        axs.set_ylabel(r'$\Delta t\ [ms]$', fontsize=35)
        print("\t{}:".format(signature))
        for key, events in key_exchange_algorithms.items():
            print("\t\t{}: ".format(key))
            for start_event in events:
                if "End" in start_event["event_name"]:
                    continue

                event_name = start_event["event_name"]
                end_event_name = event_name.replace("Start", "End")

                corresponding_end_event = None
                for end_event in events:
                    if end_event["event_name"] == end_event_name:
                        corresponding_end_event = end_event
                        break

                if corresponding_end_event == None:
                    continue

                start_time = datetime.datetime.strptime(start_event["timestamp"], '%Y-%m-%dT%H:%M:%S.%fZ')
                end_time   = datetime.datetime.strptime(corresponding_end_event["timestamp"], '%Y-%m-%dT%H:%M:%S.%fZ')
                delta      = end_time - start_time

                delta_milliseconds = round(delta.total_seconds() * 1e3, 3)
                max_delta = max(delta_milliseconds, max_delta)

                if signature not in event_time_mapping:
                    event_time_mapping[signature] = {}
                    event_time_mapping[signature][legend_mapping[event_name]] = [delta_milliseconds]
                    event_time_mapping[signature]["kx_algos"] = [key]
                else:
                    if legend_mapping[event_name] not in event_time_mapping[signature]:
                        event_time_mapping[signature][legend_mapping[event_name]] = [delta_milliseconds]
                        event_time_mapping[signature]["kx_algos"] = [key]
                    else:
                        event_time_mapping[signature][legend_mapping[event_name]].append(delta_milliseconds)
                        if key not in event_time_mapping[signature]["kx_algos"]:
                            event_time_mapping[signature]["kx_algos"].append(key)

                print("\t\t\t{} -> {}: {} ms".format(event_name, end_event_name, delta_milliseconds))

        kx_algos = event_time_mapping[signature]["kx_algos"]
        bar_colors = {
            "RequestTime": "blue",
            "ClientHandshake": "orange",
            "KeyGeneration": "green",
            "KeyDecapsulation": "red",
            "KeyEncapsulation": "red",
            "SignatureVerify": "purple",
            "ServerHandshake": "orange",
            "SignatureSign": "purple"
        }

        label_count = 0
        x_value = 0
        for kx_algo in kx_algos:
            kx_algo_index = kx_algos.index(kx_algo)
            event_names_and_time_deltas = []
            for event_name, value in event_time_mapping[signature].items():
                if event_name == "kx_algos":
                    continue

                event_names_and_time_deltas.append((value[kx_algo_index], event_name))

            event_names_and_time_deltas.sort(reverse=True, key=lambda y: y[0])
            for time_delta, event_name in event_names_and_time_deltas:
                if label_count < len(event_names_and_time_deltas):
                    axs.bar(x_value,
                            time_delta,
                            0.2,
                            label=event_name,
                            color=bar_colors[event_name])
                else:
                    axs.bar(x_value,
                            time_delta,
                            0.2,
                            color=bar_colors[event_name])

                label_count += 1

            x_value += 1

        axs.set_xticks(np.arange(len(event_time_mapping[signature]["kx_algos"])))
        axs.set_xticklabels(event_time_mapping[signature]["kx_algos"], rotation=90, fontsize=30)
        plt.yticks(fontsize=40)
        axs.legend(prop={'size': 30})
        axs.set_yscale("log")
        plt.ylim(0, 10**5)
        plt.grid(True)
        plt.title(signature + " benchmark", fontsize=40)
        fig.tight_layout()

        graphs_path = os.path.dirname(os.path.realpath(__file__)) + "/graphs/"
        if not os.path.isdir(graphs_path):
            os.mkdir(graphs_path)

        plt.savefig(graphs_path + '{}_{}.png'.format("handshake_measurement_{}".format(signature), image_postfix))

    return event_time_mapping

parser = argparse.ArgumentParser()
parser.add_argument("logfiles_path", help="Path to the directory which contains all logfiles.", type=str)
args = parser.parse_args()

if not os.path.isdir(args.logfiles_path):
    parser.print_help()
    exit()

for top_level_directory in os.walk(args.logfiles_path):
    if "client" in top_level_directory[0] or "server" in top_level_directory[0] or top_level_directory[0] == args.logfiles_path:
        continue

    server_events = {}
    client_events = {}

    for directory in [top_level_directory[0] + "/client", top_level_directory[0] + "/server"]:
        log_list = glob.glob('{}/*.log'.format(directory))
        for logfile in log_list:
            if "PRIME" in logfile or "falcon1024" in logfile:
                continue

            with open(logfile, "r") as file:
                lines = file.readlines()
                for line in lines:
                    parsed_json = json.loads(line)
                    test_name = parsed_json["fields"]["test_name"]
                    entry = {
                        "timestamp": parsed_json["timestamp"],
                        "event_name": parsed_json["fields"]["event_name"],
                        "span": parsed_json["spans"][0]["name"]
                    }
                    signature_name = re.findall("([a-zA-Z0-9]+)_", test_name)[0]
                    key_exchange_name = test_name.replace(signature_name + "_", "")

                    if "ServerEvents" in logfile:
                        if signature_name not in server_events:
                            server_events[signature_name] = {}
                            server_events[signature_name][key_exchange_name] = [entry]
                        else:
                            if key_exchange_name not in server_events[signature_name]:
                                server_events[signature_name][key_exchange_name] = [entry]
                            else:
                                server_events[signature_name][key_exchange_name].append(entry)
                    elif "ClientEvents" in logfile:
                        if signature_name not in client_events:
                            client_events[signature_name] = {}
                            client_events[signature_name][key_exchange_name] = [entry]
                        else:
                            if key_exchange_name not in client_events[signature_name]:
                                client_events[signature_name][key_exchange_name] = [entry]
                            else:
                                client_events[signature_name][key_exchange_name].append(entry)

    table_prefix = top_level_directory[0].split("/")[1]
    print("[+] Client:")
    client_event_time_mapping = calculate_time_delta(client_events, table_prefix + "_client")

    print("[+] Server: ")
    server_event_time_mapping = calculate_time_delta(server_events, table_prefix + "_server")

    tables_path = os.path.dirname(os.path.realpath(__file__)) + "/tables/"
    if not os.path.isdir(tables_path):
        os.mkdir(tables_path)

    # RequestTime ClientHandshake KeyGeneration KeyDecapsulation SignatureVerify
    client_table_begin = """\\pagestyle{empty} \\small % Switch from 12pt to 11pt; otherwise, table won't fit
    \\setlength\\LTleft{0pt}  % default: \\parindent
    \\setlength\\LTright{0pt} % default: \\fill
    \\begin{longtable}{ c c r r r r r }
    """

    # ServerHandshake KeyGeneration KeyEncapsulation SignatureSign
    server_table_begin = """\\pagestyle{empty} \\small % Switch from 12pt to 11pt; otherwise, table won't fit
    \\setlength\\LTleft{0pt}  % default: \\parindent
    \\setlength\\LTright{0pt} % default: \\fill
    \\begin{longtable}{ c c r r r r }
    """

    table_end = """
    \\end{longtable}
    """

    kx_algo_table_name_mapping = {
        "SABER": "SAB",
        "LIGHT_SABER": "LSAB",
        "FIRE_SABER": "FSAB",
        "KYBER512": "KYB512",
        "KYBER768": "KYB768",
        "KYBER1024": "KYB1024",
        "KYBER512_90S": "KYB51290S",
        "KYBER768_90S": "KYB76890S",
        "KYBER1024_90S": "KYB102490S",
        "CLASSIC_MC_ELIECE_348864": "MCEL348864",
        "CLASSIC_MC_ELIECE_348864F":  "MCEL348864F",
        "CLASSIC_MC_ELIECE_460896": "MCEL460896",
        "CLASSIC_MC_ELIECE_460896F": "MCEL460896F",
        "CLASSIC_MC_ELIECE_6688128": "MCEL6688128",
        "CLASSIC_MC_ELIECE_6688128F": "MCEL6688128F",
        "CLASSIC_MC_ELIECE_6960119": "MCEL6960119",
        "CLASSIC_MC_ELIECE_6960119F": "MCEL6960119F",
        "CLASSIC_MC_ELIECE_8192128": "MCEL8192128",
        "CLASSIC_MC_ELIECE_8192128F": "MCEL8192128F",
        "NTRU_HPS_2048509": "NTHPS2048509",
        "NTRU_HPS_2048677": "NTHPS2048677",
        "NTRU_HPS_4096821": "NTHPS4096821",
        "SECP_256_R1": "SECP256R1"
    }

    with open(tables_path + table_prefix + "_client_table.tex", "w") as client_table:
        client_table.write(client_table_begin)

        event_names_written = False
        event_name_list = []
        for signature, value in client_event_time_mapping.items():
            if event_names_written == False:
                client_table.write(" & ")
                for event_name, measurements in value.items():
                    if event_name == "kx_algos":
                        continue

                    client_table.write(" & " + event_name + " [ms]")
                    event_name_list.append(event_name)

                event_names_written = True
                client_table.write("\\\\\n\\midrule\n")

            client_table.write("\\multirow{6}{*}{" + signature + "}")
            for kx_algo in value["kx_algos"]:
                kx_acronym = kx_algo_table_name_mapping[kx_algo]
                client_table.write(" & " + kx_acronym);
                for element in event_name_list:
                    client_table.write(" & {:.3f}".format(value[element][value["kx_algos"].index(kx_algo)]))

                client_table.write("\\\\\n")

            client_table.write("\\midrule\n")

        client_table.write("\\caption{client performance measurement results}\n")
        client_table.write(table_end)

    with open(tables_path + table_prefix + "_server_table.tex", "w") as server_table:
        server_table.write(server_table_begin)

        event_names_written = False
        event_name_list = []
        for signature, value in server_event_time_mapping.items():
            if event_names_written == False:
                server_table.write(" & ")
                for event_name, measurements in value.items():
                    if event_name == "kx_algos":
                        continue

                    server_table.write(" & " + event_name + " [ms]")
                    event_name_list.append(event_name)

                event_names_written = True
                server_table.write("\\\\\n\\midrule\n")

            server_table.write("\\multirow{6}{*}{" + signature + "}")
            for kx_algo in value["kx_algos"]:
                kx_acronym = kx_algo_table_name_mapping[kx_algo]
                server_table.write(" & " + kx_acronym);
                for element in event_name_list:
                    server_table.write(" & {:.3f}".format(value[element][value["kx_algos"].index(kx_algo)]))

                server_table.write("\\\\\n")

            server_table.write("\\midrule\n")

        server_table.write("\\caption{server performance measurement results}\n")
        server_table.write(table_end)


    begin_figure = """\\begin{figure}[!htb]
    \\centering"""

    figure_body = """
            \\includegraphics[width=\\textwidth]{figure_path}
            \\caption{caption}
            \\label{label}"""
    end_figure = """\\end{figure}"""

    graphs_path = os.path.dirname(os.path.realpath(__file__)) + "/graphs/"
    if not os.path.isdir(graphs_path):
        os.mkdir(graphs_path)

    graphs_list = glob.glob('{}/*_{}_client.png'.format(graphs_path, table_prefix))

    with open("graphs/" + table_prefix + "_graphs.tex", "w") as graph_tex:
        for index in range(0, len(graphs_list)):
            graph_tex.write(begin_figure)
            relativ_path = "../code/performance_tests/graphs/" + os.path.basename(graphs_list[index])
            graph_tex.write(figure_body.format(figure_path="{" + relativ_path + "}",
                                               caption="{" + os.path.basename(relativ_path)
                                                                    .replace(".png", "")
                                                                    .replace("handshake_measurement_", "")
                                                                    .replace("_client", "") + " measurement}",
                                               label="{fig:" + os.path.basename(relativ_path)
                                                                    .replace(".png", "")
                                                                    .replace("handshake_measurement_", "")
                                                                    .replace("_client", "") + "}"))
            graph_tex.write("\n" + end_figure)








