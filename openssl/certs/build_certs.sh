#!/bin/bash

key_types=("dilithium2" "falcon512" "falcon1024" "rainbowIclassic" "rainbowIcircumzenithal" "rainbowIcompressed" "picnicl1fs" "picnicl1ur" "picnicl1full" "picnic3l1" "rainier3l1" "rainier4l1" "sphincssha256128frobust" "sphincssha256128fsimple" "sphincssha256128srobust" "sphincssha256128ssimple")
for keytype in ${key_types[*]} ; do
    mkdir certs/${keytype}

    apps/openssl req -x509 -new -newkey ${keytype} -keyout certs/${keytype}/${keytype}_${keytype}_CA.key -out certs/${keytype}/${keytype}_${keytype}_CA.crt -nodes -subj "/CN=rustls test CA" -days 365 -config certs/openssl.cnf

    apps/openssl req -new -newkey ${keytype} -keyout certs/${keytype}/${keytype}_srv.key -keyform DER -out certs/${keytype}/${keytype}_srv.csr -nodes -subj "/CN=rustls-performance.eastus.cloudapp.azure.com" -extensions v3_req -config certs/openssl.cnf

    apps/openssl x509 -req -in certs/${keytype}/${keytype}_srv.csr -out certs/${keytype}/${keytype}_srv.crt -CA certs/${keytype}/${keytype}_${keytype}_CA.crt -CAkey certs/${keytype}/${keytype}_${keytype}_CA.key -CAcreateserial -days 365 -extensions v3_end -extfile certs/openssl.cnf

    cat certs/${keytype}/${keytype}_srv.crt certs/${keytype}/${keytype}_${keytype}_CA.crt > certs/${keytype}/full.chain
done

mkdir certs/ecdsa

# ecdsa
openssl ecparam -name prime256v1 -out certs/ecdsa/nistp256.pem
openssl ecparam -name secp384r1 -out certs/ecdsa/nistp384.pem

openssl req -nodes \
          -x509 \
          -newkey ec:certs/ecdsa/nistp384.pem \
          -keyout certs/ecdsa/ca.key \
          -out certs/ecdsa/ecdsa_ecdsa_CA.crt \
          -sha256 \
          -batch \
          -days 3650 \
          -subj "/CN=ponytown ECDSA CA"

openssl req -nodes \
          -newkey ec:certs/ecdsa/nistp256.pem \
          -keyout certs/ecdsa/inter.key \
          -out certs/ecdsa/inter.req \
          -sha256 \
          -batch \
          -days 3000 \
          -subj "/CN=ponytown ECDSA level 2 intermediate"

openssl req -nodes \
          -newkey ec:certs/ecdsa/nistp256.pem \
          -keyout certs/ecdsa/ecdsa_srv.key \
          -out certs/ecdsa/end.req \
          -sha256 \
          -batch \
          -days 2000 \
          -subj "/CN=testserver.com"

openssl req -nodes \
          -newkey ec:certs/ecdsa/nistp384.pem \
          -keyout certs/ecdsa/client.key \
          -out certs/ecdsa/client.req \
          -sha256 \
          -batch \
          -days 2000 \
          -subj "/CN=ponytown client"

openssl x509 -req \
            -in certs/ecdsa/inter.req \
            -out certs/ecdsa/inter.cert \
            -CA certs/ecdsa/ecdsa_ecdsa_CA.crt \
            -CAkey certs/ecdsa/ca.key \
            -sha256 \
            -days 3650 \
            -set_serial 123 \
            -extensions v3_inter -extfile certs/openssl.cnf

openssl x509 -req \
            -in certs/ecdsa/end.req \
            -out certs/ecdsa/end.cert \
            -CA certs/ecdsa/inter.cert \
            -CAkey certs/ecdsa/inter.key \
            -sha256 \
            -days 2000 \
            -set_serial 456 \
            -extensions v3_end -extfile certs/openssl.cnf

cat certs/ecdsa/end.cert certs/ecdsa/inter.cert certs/ecdsa/ecdsa_ecdsa_CA.crt > certs/ecdsa/full.chain

