# Post-Quantum Rustls

Post-Quantum-Secure fork of [rustls](https://github.com/rustls/rustls).
The work within this repo is part of the
"Future Proofing TLS1.3: Integration and Evaluation of Post-Quantum Cryptography in Rustls" master
thesis at [IAIK - TUGraz](https://www.iaik.tugraz.at/).

## Build openssl with custom liboqs (used to generate certificates)

### Build liboqs

```
sudo apt install astyle cmake gcc ninja-build libssl-dev python3-pytest python3-pytest-xdist unzip xsltproc doxygen graphviz python3-yaml
```

```
cd liboqs
mkdir build && cd build
cmake -GNinja -DCMAKE_INSTALL_PREFIX=<OPENSSL_DIR>/oqs ..
ninja
ninja install
```

### Build openssl

```
sudo apt install cmake gcc libtool libssl-dev make ninja-build git
```

Enable the desired algorithms in [generate.yml](openssl/oqs-template/generate.yml).

Execute in openssl directory

```
export LIBOQS_SRC_DIR=../liboqs
python3 oqs-template/generate.py
./Configure no-shared linux-x86_64 -lm -lstdc++
make generate_crypto_objects
make -j 17
```

### Generate PQ certificates

* execute `bash certs/build_certs.sh`
